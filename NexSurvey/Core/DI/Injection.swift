//
//  Injection.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation

import UIKit
import RealmSwift

final class Injection: NSObject {
    
    private let realm = try? Realm()
    
    func provideLogin<U: UseCase>() -> U where U.Request == LoginRequest, U.Response == String {
        let remote = LoginRemoteDataSource(endpoint: Endpoints.Gets.login.url)
        let repository = LoginRepository(
            remoteDataSource: remote)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideSurveyGetAll<U: UseCase>() -> U where U.Request == String, U.Response == [SurveyModel] {
        let locale = SurveyLocaleDataSource(_realm: realm!)
        let remote = SurveyRemoteDataSource(endpoint: Endpoints.Gets.questions.url)
        let mapper = SurveyTransformer()
        let repository = GetAllSurveyRepository(
            surveyGetLocale: locale,
            surveyGetRemote: remote,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideSurveyGetQuestion<U: UseCase>() -> U where U.Request == String, U.Response == [PertanyaanEntity] {
        let locale = SurveyQuestionLocaleDataSource(_realm: realm!)
        let repository = GetSurveyQuestionsRepository(
            surveyGetLocale: locale)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideAnswerGetAll<U: UseCase>() -> U where U.Request == String, U.Response == [RespondenAnswerModel] {
        let locale = AnswerLocaleDataSource(_realm: realm!)
        let mapper = AnswerTransformer()
        let repository = GetAllAnswerRepository(
            answerGetLocale: locale,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideAnswerSave<U: UseCase>() -> U where U.Request == RespondenAnswerModel, U.Response == Bool {
        let localeAnswer = AnswerLocaleDataSource(_realm: realm!)
        let localeSurveyQuestion = SurveyQuestionLocaleDataSource(_realm: realm!)
        let mapper = AnswerTransformer()
        let repository = SaveAnswerRepository(
            answerGetLocale: localeAnswer,
            questionGetLocale: localeSurveyQuestion,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideAnswerDelete<U: UseCase>() -> U where U.Request == String, U.Response == Bool {
        let locale = AnswerLocaleDataSource(_realm: realm!)
        let repository = DeleteAnswerRepository(
            answerLocale: locale)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideJourneyGetAll<U: UseCase>() -> U where U.Request == String, U.Response == [JourneyModel] {
        let locale = JourneyLocaleDataSource(_realm: realm!)
        let mapper = JourneyTransformer()
        let repository = GetAllJourneyRepository(
            journeyGetLocale: locale,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideJourveySave<U: UseCase>() -> U where U.Request == JourneyModel, U.Response == Bool {
        let locale =  JourneyLocaleDataSource(_realm: realm!)
        let mapper = JourneyTransformer()
        let repository = SaveJourneyRepository(
            journeyGetLocale: locale,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideJourneyDelete<U: UseCase>() -> U where U.Request == String, U.Response == Bool {
        let locale = JourneyLocaleDataSource(_realm: realm!)
        let repository = DeleteJourneyRepository(
            journeyDeleteLocale: locale)
        
        return Interactor(repository: repository) as! U
    }
}
