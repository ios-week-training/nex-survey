//
//  ApiCall.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation

struct API {
    static let baseUrl = "https://788bd971-3106-4e25-a1d4-319d3f044cd4.mock.pstmn.io/"
//    static let baseUrl = "http://localhost:3001/"
}

protocol Endpoint {
    var url: String { get }
}

enum Endpoints {
    enum Gets: Endpoint {
        case login
        case questions
        case questionsPage
        
        public var url: String {
            switch self {
                case .login: return "\(API.baseUrl)login"
                case .questions: return "\(API.baseUrl)questions"
                case .questionsPage: return "\(API.baseUrl)questions?page="
            }
        }
    }
    
}
