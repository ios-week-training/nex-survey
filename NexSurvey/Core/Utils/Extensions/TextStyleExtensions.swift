//
//  TextStyle.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import SwiftUI

extension Text {
    
    func boldTitle() -> some View {
        self.font(.custom("Roboto-Bold", size: 24))
            .foregroundColor(Color(red: 0.07, green: 0.09, blue: 0.37, opacity: 1))
    }
    
    func fieldTitle() -> some View {
        self.font(.custom("Roboto-Medium", size: 16))
            .foregroundColor(Color(red: 0.07, green: 0.09, blue: 0.36, opacity: 1))
    }
    
    func fieldDesc() -> some View {
        self.font(.custom("Roboto-Medium", size: 14))
            .foregroundColor(Color(red: 0.17, green: 0.25, blue: 0.43, opacity: 1))
    }
    
    func surveyTitle() -> some View {
        self.font(.title2)
    }
    
    func surveyDesc() -> some View {
        self.font(.system(size: 12, weight: .regular, design: .monospaced))
            .lineSpacing(2)
            .foregroundStyle(.secondary)
    }
    
    func customButton(width: CGFloat, textColor: Color, backColor: Color) -> some View {
        self.font(.system(.callout, weight: .semibold))
            .padding()
            .frame(maxWidth: width)
            .clipped()
            .foregroundColor(textColor)
            .background(backColor)
            .mask { RoundedRectangle(cornerRadius: 16, style: .continuous) }
            .padding(.bottom, 60)
    }
}

extension TextField {
    
    func fieldText() -> some View {
        self.font(.custom("Roboto-Medium", size: 18))
            .foregroundColor(Color(red: 0.07, green: 0.09, blue: 0.36, opacity: 1))
    }
    
    func fieldDesc() -> some View {
        self.font(.custom("Roboto-Medium", size: 14))
            .foregroundColor(Color(red: 0.07, green: 0.09, blue: 0.36, opacity: 1))
    }
}

extension TextEditor {
    
    func fieldDesc() -> some View {
        self.font(.custom("Roboto-Medium", size: 14))
            .foregroundColor(Color(red: 0.07, green: 0.09, blue: 0.36, opacity: 1))
    }
}
