//
//  TabViewExtensions.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import SwiftUI

extension ReportView {
    func CustomTabItem(imageName: String = "house.fill", title: String, isActive: Bool) -> some View{
        HStack(spacing: 10){
            Spacer()
            Image(systemName: imageName)
                .resizable()
                .renderingMode(.template)
                .foregroundColor(isActive ? .black : .gray)
                .frame(width: 20, height: 20)
            if isActive{
                Text(title)
                    .font(.system(size: 14))
                    .foregroundColor(isActive ? .black : .gray)
            }
            Spacer()
        }
        .frame(maxWidth: isActive ? .infinity : max(60, 1))
        .frame(height: 60)
        .background(isActive ? .orange.opacity(0.4) : .clear)
        .cornerRadius(30)
        
    }
} 
