//
//  ColorExtensions.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import SwiftUI

extension Color {
    static var surveyBackground: Color {
        Color(.secondarySystemGroupedBackground)
    }
}

extension LinearGradient {
    static var customBlue: LinearGradient {
        LinearGradient(
            gradient: Gradient(stops: [
                .init(color: Color(red: 0.4901960790157318, green: 0.7843137383460999, blue: 0.9058823585510254, opacity: 1), location: 0),
                .init(color: Color(red: 0.4901960790157318, green: 0.7843137383460999, blue: 0.9058823585510254, opacity: 0.69), location: 1)
            ]),
            startPoint: UnitPoint(x: 0.5, y: 1.5),
            endPoint: UnitPoint(x: 1.5, y: 0.5)
        )
    }
    
    static var customGreen: LinearGradient {
        LinearGradient(
            gradient: Gradient(stops: [
                .init(color: Color(red: 0.5058823823928833, green: 0.9098039269447327, blue: 0.6189804077148438, opacity: 1), location: 0),
                .init(color: Color(red: 0.5058823823928833, green: 0.9098039269447327, blue: 0.6196078658103943, opacity: 0.3499999940395355), location: 1)
            ]),
            startPoint: UnitPoint(x: 0.5, y: 1.5),
            endPoint: UnitPoint(x: 1.5, y: 0.5)
        )
    }
    
    static var surveyBackground: LinearGradient {
        let color = Color(UIColor.secondarySystemGroupedBackground)
        return LinearGradient(gradient: Gradient(colors: [color, color]), startPoint: .top, endPoint: .bottom)
    }
    
    static var reportBackground: LinearGradient {
        let color = Color(red: 0.9751389026641846, green: 0.9807755351066589, blue: 0.9916666746139526, opacity: 1)
        return LinearGradient(gradient: Gradient(colors: [color, color]), startPoint: .top, endPoint: .bottom)
    }
}
