//
//  TabEnum.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import Foundation

enum TabbedItems: Int, CaseIterable{
    case report = 0
    case location
    
    var title: String{
        switch self {
        case .report:
            return "List Koresponden"
        case .location:
            return "Location Journey"
        }
    }
    
    var icon: String {
        switch self {
        case .report:
            return "house.fill"
        case .location:
            return "map.fill"
        }
    }
}
