//
//  CameraAuth.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 26/03/24.
//

import Foundation
import AVFoundation

func checkAuth() -> Bool {
    let status = AVCaptureDevice.authorizationStatus(for: .video)

    switch status {
        case .authorized:
            print("Camera access authorized")
            return false
        case .notDetermined:
            print("Camera access not determined. Requesting access...")
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    print("Granted access to camera")
                } else {
                    print("Denied access to camera")
                }
            }
            return false
        case .denied:
            print("Camera access denied")
            return true
        case .restricted:
            print("Camera access restricted")
            return true
        @unknown default:
            fatalError("Unknown camera authorization status")
    }
}
