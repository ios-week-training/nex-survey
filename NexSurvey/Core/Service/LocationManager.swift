//
//  LocationManager.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 22/03/24.
//

import SwiftUI
import Foundation
import CoreLocation
import MapKit

final class LocationManager: NSObject, CLLocationManagerDelegate, ObservableObject, MKMapViewDelegate {
    @ObservedObject var journeySavePresenter = SaveJourneyPresenter(saveJourney: journeySaveUseCase)
    let locationManager = CLLocationManager()
    private var lastUpdateTimestamp: Date?
    private let timeCycle: Double = 10
    @Published var isLocationUnAuthorized: Bool = false
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(
        _ manager: CLLocationManager,
        didChangeAuthorization status: CLAuthorizationStatus
    ) {
        checkLocationAuthorization()
    }

    func locationManager(
        _ manager: CLLocationManager,
        didUpdateLocations locations: [CLLocation]
    ) {
        let now = Date()
        guard let lastUpdate = lastUpdateTimestamp, now.timeIntervalSince(lastUpdate) <= timeCycle else {
            lastUpdateTimestamp = now
            if let location = locations.last {
                saveLocation(date: now, location: location)
            }
            return
        }
    }

    func locationManager(
        _ manager: CLLocationManager,
        didFailWithError error: Error
    ) {
        // Handle failure to get a user’s location
    }
    
    func checkLocationAuthorization() {
        switch locationManager.authorizationStatus {
        case .notDetermined:
            print("not determined")
            DispatchQueue.main.async {
                self.locationManager.requestAlwaysAuthorization()
            }
        case .restricted, .denied:
            print("Location Authorization Denied or Restricted")
            isLocationUnAuthorized = true
        case .authorizedWhenInUse, .authorizedAlways:
            print("auth always")
            isLocationUnAuthorized = false
            DispatchQueue.main.async {
                self.locationManager.startUpdatingLocation()
            }
        default:
            print("default")
            break
        }
    }
    
    private func saveLocation(date: Date, location: CLLocation) {
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        journeySavePresenter.saveJourney(date: date, latitude: latitude, longitude: longitude)
        print(journeySavePresenter.result)
    }
}
