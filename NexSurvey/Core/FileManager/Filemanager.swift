//
//  Filemanager.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 26/03/24.
//

import Foundation
import UIKit

struct FileUrl {
    static let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
}
