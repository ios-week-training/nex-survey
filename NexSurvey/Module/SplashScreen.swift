//
//  SplashScreen.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

struct SplashScreen: View {
    var body: some View {
        VStack {
            Image("Logo")
                .resizable()
                .frame(width: 325, height: 100)
        }
    }
}

#Preview {
    SplashScreen()
}
