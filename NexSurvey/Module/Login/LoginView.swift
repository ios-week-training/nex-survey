//
//  LoginView.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject var presenter: LoginPresenter<Interactor<LoginRequest, String, LoginRepository<LoginRemoteDataSource>>>
    
    var body: some View {
        
        NavigationStack {
            ZStack {
                VStack {
                    Image("Logo")
                        .resizable()
                        .frame(width: 250, height: 80)
                        .padding(.bottom, 70)
                    Text(presenter.errorMessage)
                        .font(.subheadline)
                        .foregroundStyle(Color.red)
                        .italic()
                    Group {
                        InputField(text: $presenter.username, placeholder: "Username")
                        InputField(text: $presenter.password, placeholder: "Password", isPasswordField: true)
                        
                    }
                    PrimaryButton(title: "Sign In")
                        .padding(.vertical)
                        .onTapGesture {
                            presenter.postLogin()
                        }
                        .navigationDestination(
                            isPresented: $presenter.isLogin
                        ) {
                            DashboardView()
                            Text("")
                                .hidden()
                        }
                }
                .padding()
            }
        }
        .overlay {
            if presenter.isLoading {
                ZStack {
                    Rectangle()
                        .foregroundStyle(Color.gray)
                        .opacity(0.2)
                    ProgressView()
                }
            }
        }
        .onAppear {
            presenter.isLogin = Auth.shared.login()
        }
    }
}

//#Preview {
//    LoginView()
//}
