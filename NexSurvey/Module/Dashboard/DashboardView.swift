//
//  DashboardView.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

struct DashboardView: View {
    let surveyGetAllPresenter = SurveyGetAllPresenter(
        surveyGetAll: surveyUseCase)
    let reportGetAllPresenter = GetAllAnswerPresenter(
        getAllAnswer: answerListUseCase)
    @StateObject var locationManager: LocationManager = .init()
    
    var body: some View {
        VStack {
            LogoTitle()
            Text("Dashboard")
                .boldTitle()
                .padding(.bottom, 20)
            CredentialInfo()
                .padding(.bottom)
            VStack {
                NavigationLink {
                    SurveyView(surveyGetAllPresenter: surveyGetAllPresenter)
                } label: {
                    ColorMenuCard(color: LinearGradient.customBlue, minHeight: 100, alignment: .center) {
                        HStack{
                            Image(systemName: "list.clipboard.fill")
                                .resizable()
                                .frame(width: 36, height: 40)
                                .foregroundStyle(Color.black)
                            Text("Fill Survey")
                                .boldTitle()
                            Spacer()
                            Image("DocLogo")
                                .resizable()
                                .frame(width: 80, height: 80)
                        }
                    }
                }
                NavigationLink {
                    ReportView(answerGetAllPresenter: reportGetAllPresenter)
                } label: {
                    ColorMenuCard(color: LinearGradient.customGreen, minHeight: 100, alignment: .center) {
                        HStack{
                            Image(systemName: "doc.on.clipboard")
                                .resizable()
                                .frame(width: 38, height: 40)
                                .foregroundStyle(Color.black)
                            Text("Report List")
                                .boldTitle()
                            Spacer()
                            Image("DocLogo")
                                .resizable()
                                .frame(width: 80, height: 80)
                        }
                    }
                }
            }
        }
        Spacer()
            .alert("Please Allow Location Access", isPresented: $locationManager.isLocationUnAuthorized) {
                Button("Open Setting") {
                    guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        .toolbar(.hidden, for: .navigationBar)
    }
}

#Preview {
    DashboardView()
}
