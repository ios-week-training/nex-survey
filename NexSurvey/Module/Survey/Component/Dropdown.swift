//
//  Dropdown.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct Dropdown: View {
    @Binding var selection: String
    var question: String
    var options: [String]
    var isRequired: Bool
    @State var showDropdown = false
    @Binding var dropPadding: CGFloat
    var isReport: Bool = false
    
    @SceneStorage("drop_down_zindex") private var index = 1000.0
    @State var zindex = 100.0
    
    var body: some View {
        VStack(alignment: .leading) {
            Question(isRequired: isRequired, question: question)
            GeometryReader {
                let size = $0.size
                VStack(spacing: 0) {
                    HStack {
                        Text(selection == "" ? "Select" : selection)
                            .fieldDesc()
                            .foregroundColor(selection != "" ? .black : .gray)
                        Spacer(minLength: 0)
                        Image(systemName: "chevron.down")
                            .font(.title3)
                            .foregroundColor(.gray)
                            .rotationEffect(.degrees((showDropdown ? -180 : 0)))
                    }
                    .padding(.horizontal, 15)
                    .frame(height: 50)
                    .background(.white)
                    .contentShape(.rect)
                    .onTapGesture {
                        index += 1
                        zindex = index
                        withAnimation(.snappy) {
                            showDropdown.toggle()
                            dropPadding = CGFloat(dropPadding == 0 ? (options.count * 40) : 0)
                        }
                    }
                    .zIndex(1000)
                    
                    if showDropdown && !isReport {
                        OptionsView()
                    }
                }
                .clipped()
                .background(.white)
                .cornerRadius(10)
                .overlay {
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(.gray)
                }
                .frame(height: size.height, alignment: .top)
                
            }
            .frame(height: 50)
            .zIndex(zindex)
        }
        .zIndex(showDropdown ? 1 : 0)
    }


    func OptionsView() -> some View {
        VStack(spacing: 0) {
            ForEach(options, id: \.self) { option in
                HStack {
                    Text(option)
                    Spacer()
                    Image(systemName: "checkmark")
                        .opacity(selection == option ? 1 : 0)
                }
                .foregroundStyle(selection == option ? Color.primary : Color.gray)
                .animation(.none, value: selection)
                .frame(height: 40)
                .contentShape(.rect)
                .padding(.horizontal, 15)
                .onTapGesture {
                    withAnimation(.snappy) {
                        selection = option
                        showDropdown.toggle()
                        dropPadding = CGFloat(dropPadding == 0 ? (options.count * 40) : 0)
                    }
                }
            }
        }
        .transition(.move(edge: .top))
        .zIndex(showDropdown ? 1 : 0)
    }
}

//#Preview {
//    Dropdown(selection: .constant(""), question: "Pertanyaan 3", options: [
//        "Apple",
//        "Google",
//        "Amazon",
//        "Facebook",
//        "Instagram"
//    ], isRequired: true)
//}
