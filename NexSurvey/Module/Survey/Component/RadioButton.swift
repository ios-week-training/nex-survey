//
//  Radioibutton.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import SwiftUI

struct RadioButton: View {
    @ObservedObject var presenter: SurveyPresenter<Interactor<String, [PertanyaanEntity], GetSurveyQuestionsRepository<SurveyQuestionLocaleDataSource>>>
    let question: String
    let opsi: [String]
    var isRequired: Bool
    
    var body: some View {
        Question(isRequired: isRequired, question: question)
        VStack(alignment: .leading, spacing: 8) {
            ForEach(opsi, id: \.self) { item in
                let key = "\(question)-\(item)"
                HStack {
                    Image(systemName:
                            presenter.checkedAnswer[key, default: false] ? "checkmark.circle.fill" : "circle")
                    Text(item)
                        .fieldDesc()
                }
                .onTapGesture {
                    toggleActive(key)
                }
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    private func toggleActive(_ select: String) {
        for item in opsi {
            let key = "\(question)-\(item)"
            presenter.checkedAnswer[key] = false
        }
        presenter.checkedAnswer[select] = true
    }
}

//#Preview {
//    RadioButton()
//}
