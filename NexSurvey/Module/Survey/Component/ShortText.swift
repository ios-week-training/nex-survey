//
//  ShortText.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import SwiftUI

struct ShortText: View {
    var question: String
    @Binding var answer: String
    var isReport: Bool = false
    var isRequired: Bool
    
    var body: some View {
        VStack(alignment: .leading) {
            Question(isRequired: isRequired, question: question)
            TextField("Answer", text: $answer)
                .fieldDesc()
                .disabled(isReport)
            Divider()
        }
    }
}

#Preview {
    ShortText(question: "Pertanyaan 1", answer: .constant(""), isRequired: true)
}
