//
//  Question.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import SwiftUI

struct Question: View {
    var isRequired: Bool
    let question: String
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text(question)
                .fieldTitle()
            if isRequired {
                Image(systemName: "staroflife.fill")
                    .resizable()
                    .frame(width: 10, height: 10)
                    .foregroundStyle(Color.red)
            }
            Spacer()
        }
    }
}

#Preview {
    Question(isRequired: true, question: "PertanyaanPertanyaanPertanyaanPertanyaanPertanyaanPertanyaanPertanyaanPertanyaan")
}
