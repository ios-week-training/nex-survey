//
//  Checkbox.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import SwiftUI

struct Checkbox: View {
    @ObservedObject var presenter: SurveyPresenter<Interactor<String, [PertanyaanEntity], GetSurveyQuestionsRepository<SurveyQuestionLocaleDataSource>>>
    let question: String
    let opsi: [String]
    var isRequired: Bool

    var body: some View {
        Question(isRequired: isRequired, question: question)
        ForEach(opsi, id: \.self) { item in
            Toggle(isOn: presenter.bindingCheckbox(for: "\(question)-\(item)")) {
                Text(item)
                    .fieldDesc()
            }
            .toggleStyle(CheckboxExtensions())
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

//#Preview {
//    Checkbox(
//        checkedStates: .constant(["": false]), question: "Pertanyaan 1",
//        opsi: ["Product A", "Product B", "Product C"])
//}
