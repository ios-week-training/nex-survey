//
//  CheckboxItem.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import SwiftUI

struct CheckboxItem: View {
    @Binding var isChecked: Bool
    var item: String
    
    var body: some View {
        Toggle(isOn: $isChecked) {
            Text(item)
                .fieldDesc()
        }
        .toggleStyle(CheckboxExtensions())
    }
}

#Preview {
    CheckboxItem(isChecked: .constant(false), item: "Product A")
}
