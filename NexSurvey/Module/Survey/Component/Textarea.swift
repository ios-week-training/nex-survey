//
//  Textarea.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import SwiftUI

struct Textarea: View {
    var question: String
    @Binding var answer: String
    var isReport: Bool = false
    var isRequired: Bool
    
    var body: some View {
        VStack(alignment: .leading) {
            Question(isRequired: isRequired, question: question)
            ZStack(alignment: .topLeading) {
                TextEditor(text: $answer)
                    .fieldDesc()
                    .frame(minHeight: 30, alignment: .leading)
                    .overlay(
                        Group {
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(lineWidth: 1)
                            HStack {
                                Text(answer == "" ? "Answer ... ": "")
                                    .fieldDesc()
                                    .padding(.leading, 5)
                                    .foregroundStyle(.opacity(answer == "" ? 0.3 : 0))
                                Spacer()
                            }
                        }
                    )
                    .disabled(isReport)
            }
        }
    }
}

#Preview {
    Textarea(question: "Pertanyaan 1", answer: .constant(""), isRequired: true)
}
