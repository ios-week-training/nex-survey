//
//  Camera.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 24/03/24.
//

import SwiftUI

struct Camera: View {
    @State var takePicture: Bool = false
    var question: String
    var isRequired: Bool
    var isReport: Bool = false
    @Binding var imagePhoto: UIImage?
    var imageUrl: String?
    @State var newImage: UIImage?
    @State var isDenied: Bool = false
    
    private var displayImage: Image? {
       if let image = imagePhoto ?? newImage {
           return Image(uiImage: image)
       } else {
           return nil
       }
   }
    
    var body: some View {
        VStack(alignment: .leading) {
            Question(isRequired: isRequired, question: question)
            if let image = displayImage {
                image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: 400)
                    .clipped()
            }
            if !isReport {
                RoundedRectangle(cornerRadius: 15)
                    .frame(height: 60)
                    .foregroundStyle(Color.gray)
                    .opacity(0.2)
                    .overlay {
                        HStack() {
                            Image(systemName: "camera.fill")
                            Text(imagePhoto != nil ? "Change Photo" : "Take Photo")
                            Spacer()
                        }
                        .padding()
                    }
                    .onTapGesture {
                        self.isDenied = checkAuth()
                        if !isDenied {
                            self.takePicture.toggle()
                        }
                    }
                    .alert("Please Allow Camera Access", isPresented: $isDenied) {
                        Button("Open Setting") {
                            guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                    .fullScreenCover(isPresented: self.$takePicture) {
                        CameraService(selectedImage: $imagePhoto)
                    }
            }
        }
        .onAppear {
            if let url = imageUrl {
                var images: UIImage? = nil
                let load = FileUrl.baseUrl.appendingPathComponent(url)
                load.loadImage(&images)
                self.newImage = images
            }
        }
    }
}

//#Preview {
//    Camera(question: "Pertanyaan", isRequired: true, imagePhoto: .constant(nil))
//}
