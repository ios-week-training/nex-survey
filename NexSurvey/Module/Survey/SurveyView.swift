//
//  SurveyView.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI
import RealmSwift

struct SurveyView: View {
    @ObservedObject var surveyGetAllPresenter: SurveyGetAllPresenter<Interactor<String, [SurveyModel],GetAllSurveyRepository<SurveyLocaleDataSource, SurveyRemoteDataSource, SurveyTransformer>>>
    let surveyQuestionPresenter = SurveyPresenter(
        surveyQuestion: surveyQuestionUseCase)
    
    var body: some View {
        VStack {
            BackButton()
            ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .center) {
                Text("Survey Responden")
                    .surveyTitle()
            }
            ScrollViewReader { reader in
                SurveyItem(surveyPresenter: surveyQuestionPresenter, question: surveyGetAllPresenter.result, reader: reader)
            }
        }
        .toolbar(.hidden, for: .navigationBar)
        .onAppear {
            print(Realm.Configuration.defaultConfiguration.fileURL!)
            surveyGetAllPresenter.getAllSurvey()
        }
    }
}

//#Preview {
//    SurveyView(
//        getAllDataPresenter: SurveyGetAllPresenter(surveyGetAll: surveyUseCase)
//    )
//}
