//
//  SurveyButton.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import SwiftUI

struct SurveyButton: View {
    let changePage: (Int) -> Void
    @Binding var activePage: Int
    var surveyPage:Int
    let buttonWidth: CGFloat = 125
    let submit: () -> Void
    var isReport: Bool = false
    
    var body: some View {
        if activePage == 0 {
            HStack {
                Spacer()
                Text("Next Page")
                    .customButton(width: buttonWidth, textColor: .white, backColor: .blue)
                    .onTapGesture {
                        changePage(1)
                    }
            }
        } else if activePage == surveyPage {
            HStack{
                Text("Back")
                    .customButton(width: buttonWidth, textColor: .white, backColor: .gray)
                    .onTapGesture {
                        changePage(-1)
                    }
                Spacer()
                if !isReport {
                    Text("Submit")
                        .customButton(width: buttonWidth, textColor: .white, backColor: .green)
                        .onTapGesture {
                            submit()
                        }
                }
            }
        } else {
            HStack{
                Text("Back")
                    .customButton(width: buttonWidth, textColor: .black, backColor: .gray)
                    .onTapGesture {
                        changePage(-1)
                    }
                Spacer()
                Text("Next Page")
                    .customButton(width: buttonWidth, textColor: .white, backColor: .blue)
                    .onTapGesture {
                        changePage(1)
                    }
            }
        }
    }
}

//#Preview {
//    SurveyButton()
//}
