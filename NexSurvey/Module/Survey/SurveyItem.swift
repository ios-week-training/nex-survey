//
//  SurveyItem.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import SwiftUI

struct SurveyItem: View {
    @ObservedObject var surveyPresenter: SurveyPresenter<Interactor<String, [PertanyaanEntity], GetSurveyQuestionsRepository<SurveyQuestionLocaleDataSource>>>
    let answerSavePresenter = SaveAnswerPresenter(answer: answerSaveUseCase)
    let question: [SurveyModel]
    let reader: ScrollViewProxy
    @Environment(\.dismiss) var dismiss
    @State var paddingData: CGFloat = 0
    @State var isFinish: Bool = false
    
    var body: some View {
        ScrollView {
            ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .center) {
                SurveyInputField(placeholder: "Name", value: surveyPresenter.bindingAnser(for: "name"))
                SurveyInputField(placeholder: "Age", value: surveyPresenter.bindingAnser(for: "age"))
            }
            .id("topScrollPoint")
            .padding(.bottom)
            
            if surveyPresenter.activePage < question.count {
                let actPage = surveyPresenter.activePage
                let data = question[actPage]
                ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
                    Text(data.judul)
                        .boldTitle()
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Text(data.deskripsi)
                        .surveyDesc()
                }
                
                ForEach(data.pertanyaan, id: \.self) { item in
                    ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
                        switch item.jenis {
                        case .camera:
                            Camera(question: item.pertanyaan, isRequired: item.wajib, imagePhoto: surveyPresenter.bindingImage(for: item.pertanyaan))
                        case .checkbox:
                            Checkbox(presenter: surveyPresenter, question: item.pertanyaan, opsi: item.opsi ?? [], isRequired: item.wajib)
                        case .dropdown:
                            Dropdown(selection: surveyPresenter.bindingAnser(for: item.pertanyaan), question: item.pertanyaan, options: item.opsi ?? [], isRequired: item.wajib, dropPadding: surveyPresenter.bindingDropdown(for: item.pertanyaan))
                                .padding(.bottom, surveyPresenter.dropPadding[item.pertanyaan])
                        case .freeText:
                            Textarea(question: item.pertanyaan, answer: surveyPresenter.bindingAnser(for: item.pertanyaan), isRequired: item.wajib)
                        case .radio:
                            RadioButton(presenter: surveyPresenter, question: item.pertanyaan, opsi: item.opsi ?? [], isRequired: item.wajib)
                        case .shortAnswer:
                            ShortText(question: item.pertanyaan, answer: surveyPresenter.bindingAnser(for: item.pertanyaan), isRequired: item.wajib)
                        }
                    }
                }
            } else {
                EmptyView()
            }
            
            SurveyButton(
                changePage: surveyPresenter.changePage(_:),
                activePage: $surveyPresenter.activePage,
                surveyPage: question.count - 1,
                submit: surveyPresenter.setupAnswer
            )
            .padding()
        }
        .onAppear {
            surveyPresenter.isSubmited = false
            surveyPresenter.activePage = 0
        }
        .onChange(of: surveyPresenter.activePage) { newValue in
            withAnimation {
                reader.scrollTo("topScrollPoint", anchor: .bottom)
            }
        }
        .alert("Are you sure want to submit answer", isPresented: $surveyPresenter.isSubmited) {
            Button("Sumbit") {
                answerSavePresenter.saveAnswer(survey: question, stringAnswer: surveyPresenter.answer, checkedAnswer: surveyPresenter.checkedAnswer, imageAnswer: surveyPresenter.imageAnswer)
                self.isFinish = true
            }
            Button("Cancel", role: .cancel) { }
        } message: {
            Text("Your action cannot be changed later, and this action cannot be undone")
        }
        .alert("Please fill all mandatory field", isPresented: $surveyPresenter.status) {
            Button("Okay", role: .cancel) {
                surveyPresenter.status = false
            }
        }
        .alert("Success Save Survey", isPresented: $isFinish) {
            Button("Okay") {
                dismiss()
                surveyPresenter.isSubmited = false
                surveyPresenter.reset()
            }
        }
        .overlay {
            if answerSavePresenter.isLoading {
                ZStack {
                    Rectangle()
                        .foregroundStyle(Color.gray)
                        .opacity(0.2)
                    ProgressView()
                }
            }
        }
    }
}

//#Preview {
//    SurveyItem()
//}
