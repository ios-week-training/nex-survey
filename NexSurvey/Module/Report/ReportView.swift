//
//  ReportView.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct ReportView: View {
    @State var selectedTab = 0
    @ObservedObject var answerGetAllPresenter: GetAllAnswerPresenter<Interactor<String, [RespondenAnswerModel], GetAllAnswerRepository<AnswerLocaleDataSource, AnswerTransformer>>>
    @ObservedObject var deleteJourneyPresenter: DeleteJourneyPresenter<Interactor<String, Bool, DeleteJourneyRepository<JourneyLocaleDataSource>>> = DeleteJourneyPresenter(deleteJourney: journeyDeleteUseCase)
        
    var body: some View {
        VStack {
            HStack {
                BackButton()
                if selectedTab == 1 {
                    Text("Clear journey")
                        .onTapGesture {
                            deleteJourneyPresenter.deleteJourney()
                        }
                        .padding(.trailing)
                }
            }
            ZStack(alignment: .bottom){
                TabView(selection: $selectedTab) {
                    ListReport(answerGetAllPresenter: answerGetAllPresenter)
                        .tag(0)
                    LocationJourney()
                        .tag(1)
                }
                
                ZStack{
                    RoundedRectangle(cornerRadius: 20)
                        .fill(Color("Custom-Gray"))
                        .frame(maxWidth: .infinity)
                        .frame(height: 74)
                    HStack{
                        ForEach((TabbedItems.allCases), id: \.self){ item in
                            Button{
                                selectedTab = item.rawValue
                            } label: {
                                CustomTabItem(imageName: item.icon, title: item.title, isActive: (selectedTab == item.rawValue))
                            }
                        }
                    }
                    .padding(6)
                }
                .frame(height: 70)
                .cornerRadius(35)
                .padding(.horizontal, 26)
            }
            .overlay {
                if deleteJourneyPresenter.isLoading {
                    ZStack {
                        Rectangle()
                        ProgressView()
                    }
                }
            }
            .onAppear {
                answerGetAllPresenter.getAllAnswer()
            }
            .toolbar(.hidden, for: .navigationBar, .tabBar)
            .toolbarBackground(.hidden, for: .tabBar)
        }
    }
}

//#Preview {
//    ReportView()
//}
