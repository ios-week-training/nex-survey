//
//  LocationJourney.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI
import MapKit

struct LocationJourney: View {
    @ObservedObject var journeyGetAllPresenter = GetAllJourneyPresenter(getAllJourney: journeyGetAllUseCase)
    
    var body: some View {
        VStack {
            if journeyGetAllPresenter.result.count > 0, let coor = journeyGetAllPresenter.selectedRoute {
                MapViewWithPolylines(coordinate: coor, routes: journeyGetAllPresenter.route)
            } else {
                ProgressView()
            }
        }
        .onAppear {
            journeyGetAllPresenter.calculateRoute()
        }
    }
}

#Preview {
    LocationJourney()
}
