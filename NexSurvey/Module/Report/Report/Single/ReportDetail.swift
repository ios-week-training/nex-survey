//
//  ReportDetail.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct ReportDetail: View {
    @ObservedObject var deleteAnswerPresenter: DeleteAnswerPresenter<Interactor<String, Bool, DeleteAnswerRepository<AnswerLocaleDataSource>>>
    var answerList: RespondenAnswerModel
    @State var pageNumber: Int = 0
    @State var isDelete: Bool = false
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            HStack {
                BackButton()
                HStack {
                    Image(systemName: "trash.fill")
                        .foregroundStyle(Color.red)
                    Text("Delete Report")
                        .padding(.trailing)
                }
                .onTapGesture {
                    self.isDelete = true
                }
            }
            ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
                HStack(alignment: .firstTextBaseline, spacing: 9) {
                    Text(answerList.name)
                        .font(.title2)
                    Spacer()
                }
                HStack(alignment: .firstTextBaseline) {
                    Text(answerList.age)
                        .font(.system(size: 12, weight: .regular, design: .monospaced))
                        .lineSpacing(2)
                        .foregroundColor(.secondary)
                    Spacer()
                }
                HStack(alignment: .firstTextBaseline) {
                    Text("\(answerList.date)")
                        .font(.system(size: 12, weight: .regular, design: .monospaced))
                        .lineSpacing(2)
                        .foregroundColor(.secondary)
                    Spacer()
                }
            }
            ScrollViewReader { reader in
                ScrollView {
                    ReportItem(pageNumber: $pageNumber, answer: answerList.surveyList[pageNumber])
                    
                    SurveyButton(changePage: changePage(act:), activePage: $pageNumber, surveyPage: answerList.surveyList.count - 1, submit: {}, isReport: true)
                        .padding()
                }
                .onChange(of: pageNumber) { newValue in
                    withAnimation {
                        reader.scrollTo("topScrollPoint", anchor: .bottom)
                    }
                }
            }
        }
        .alert("Are you sure want to delete this item", isPresented: $isDelete) {
            Button("Delete") {
                deleteAnswerPresenter.deleteAnswer(id: answerList.id, answer: answerList.surveyList)
            }
        } message: {
            Text("Your action cannot be undone")
        }
        .alert("Success delete survey", isPresented: $deleteAnswerPresenter.isSuccess) {
            Button("OK") {
                dismiss()
            }
        }
        .toolbar(.hidden, for: .navigationBar)
    }
    
    private func changePage(act: Int) {
        pageNumber += act
    }
}

//#Preview {
//    ReportDetail()
//}
