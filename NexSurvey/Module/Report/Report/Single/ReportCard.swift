//
//  ReportCard.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct ReportCard: View {
    var name: String
    var age: String
    
    var body: some View {
        HStack {
            VStack(spacing: 8){
                HStack {
                    Text("Name:")
                    Text(name)
                        .fieldTitle()
                    Spacer()
                }
                HStack {
                    Text("Age:")
                    Text("\(age)")
                        .fieldTitle()
                    Spacer()
                }
            }
            Image(systemName: "chevron.right")
        }
        .padding()
    }
}

#Preview {
    ReportCard(name: "Iqbal Rahman", age: "21")
}
