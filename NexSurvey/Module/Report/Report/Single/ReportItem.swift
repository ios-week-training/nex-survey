//
//  ReportItem.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import SwiftUI

struct ReportItem: View {
    @Binding var pageNumber: Int
    let answer: SurveyAnswerModel
    
    var body: some View {
        ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
            HStack(alignment: .firstTextBaseline, spacing: 9) {
                Text(answer.surveyQuestion)
                    .font(.title2)
                Spacer()
            }
            HStack(alignment: .firstTextBaseline) {
                Text(answer.surveyDesc)
                    .font(.system(size: 12, weight: .regular, design: .monospaced))
                    .lineSpacing(2)
                    .foregroundColor(.secondary)
            }
            .id("topScrollPoint")
        }
        ForEach(answer.answerList) { item in
            ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
                let data = item.question ?? PertanyaanModel()
                switch data.jenis {
                case .camera:
                    Camera(question: data.pertanyaan, isRequired: data.wajib, isReport: true, imagePhoto: .constant(nil), imageUrl: item.answer.first)
                case .checkbox:
                    CheckboxReport(question: data.pertanyaan, opsi: data.opsi ?? [], answer: item.answer)
                case .dropdown:
                    Dropdown(selection: .constant(item.answer.first ?? ""), question: data.pertanyaan, options: data.opsi ?? [], isRequired: false, dropPadding: .constant(0), isReport: true)
                case .freeText:
                    Textarea(question: data.pertanyaan, answer: .constant(item.answer.first ?? ""), isReport: true, isRequired: false)
                case .radio:
                    RadioButtonReport(question: data.pertanyaan, opsi: data.opsi ?? [], answer: item.answer.first ?? "")
                case .shortAnswer:
                    ShortText(question: data.pertanyaan, answer: .constant(item.answer.first ?? ""), isReport: true, isRequired: false)
                }
            }
        }
    }
}

//#Preview {
//    ReportItem(pageNumber: .constant(0))
//}
