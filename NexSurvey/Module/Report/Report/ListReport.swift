//
//  ListReport.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct ListReport: View {
    @ObservedObject var answerGetAllPresenter: GetAllAnswerPresenter<Interactor<String, [RespondenAnswerModel], GetAllAnswerRepository<AnswerLocaleDataSource, AnswerTransformer>>>
    let deleteAnswerPresenter = DeleteAnswerPresenter(answer: answerDeleteUseCase)
    
    var body: some View {
        VStack(alignment: .leading) {
            ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .center) {
                Text("Responden List")
                    .boldTitle()
            }
            NavigationView {
                ScrollView {
                    let report = answerGetAllPresenter.result
                    ForEach(report) { item in
                        NavigationLink {
                            ReportDetail(deleteAnswerPresenter: deleteAnswerPresenter, answerList: item)
                        } label: {
                            ColorMenuCard(color: LinearGradient.reportBackground, minHeight: 0, alignment: .leading) {
                                ReportCard(name: item.name, age: item.age)
                            }
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
            Spacer()
        }
        .toolbarBackground(.hidden, for: .tabBar)
    }
}

//#Preview {
//    ListReport()
//}
