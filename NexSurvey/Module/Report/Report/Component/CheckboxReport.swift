//
//  Checkbox.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import SwiftUI

struct CheckboxReport: View {
    let question: String
    let opsi: [String]
    let answer: [String]

    var body: some View {
        VStack(alignment: .leading) {
            Text(question)
                .fieldTitle()
            ForEach(Array(zip(opsi.indices, opsi)), id: \.0) { index, item in
                Toggle(isOn: .constant(answer.contains(item))) {
                    Text(item)
                        .fieldDesc()
                }
                .toggleStyle(CheckboxExtensions())
                .disabled(true)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

//#Preview {
//    CheckboxReport()
//}
