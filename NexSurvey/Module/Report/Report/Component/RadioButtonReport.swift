//
//  RadioButtonReport.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import SwiftUI

struct RadioButtonReport: View {
    let question: String
    let opsi: [String]
    let answer: String
    
    var body: some View {
        Text(question)
            .fieldTitle()
        VStack(alignment: .leading, spacing: 8) {
            ForEach(Array(zip(opsi.indices, opsi)), id:  \.0) { index, item in
                HStack {
                    Image(systemName: answer == item ? "checkmark.circle.fill" : "circle")
                    Text(item)
                        .fieldDesc()
                }
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

//#Preview {
//    RadioButtonReport()
//}
