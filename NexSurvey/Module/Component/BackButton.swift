//
//  BackButton.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct BackButton: View {
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        HStack {
            Group {
                RoundedRectangle(cornerRadius: 14)
                    .fill(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
                    .frame(width: 36, height: 36)
                    .shadow(color: Color(red: 0.9458333253860474, green: 0.9674999713897705, blue: 1, opacity: 1), radius:13, x:-3, y:7)
                    .overlay {
                        Image(systemName: "chevron.left")
                            .bold()
                    }
                Text("Back")
                    .fieldTitle()
            }
            .onTapGesture {
                dismiss()
            }
            
            Spacer()
        }
        .padding()
    }
}

#Preview {
    BackButton()
}
