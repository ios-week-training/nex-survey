//
//  SurveyInputField.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct SurveyInputField: View {
    var placeholder: String = ""
    @Binding var value: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(placeholder)
                .fieldTitle()
            TextField("Enter your \(placeholder.lowercased())", text: $value)
                .fieldText()
            Divider()
        }
    }
}

#Preview {
    SurveyInputField(placeholder: "Name", value: .constant(""))
}
