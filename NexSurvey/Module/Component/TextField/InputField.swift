//
//  InputField.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

struct InputField: View {
    @Binding var text: String
    var placeholder: String
    var isPasswordField: Bool = false
    @State private var showPassword: Bool = false

    var body: some View {
        ZStack(alignment: .trailing) {
            if isPasswordField && !showPassword {
                SecureField(placeholder, text: $text)
                    .font(.title3)
            } else {
                TextField(placeholder, text: $text)
                    .font(.title3)
            }

            if isPasswordField {
                Image(systemName: showPassword ? "eye.slash.fill" : "eye.fill")
                    .foregroundColor(.gray)
                    .onTapGesture {
                        showPassword.toggle()
                    }
                    .padding(.trailing, 15)
            }
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(50.0)
        .shadow(color: Color.black.opacity(0.08), radius: 60, x: 0, y: 16)
    }
}

// Preview
struct InputField_Previews: PreviewProvider {
    static var previews: some View {
        InputField(text: .constant(""), placeholder: "Password", isPasswordField: true)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
