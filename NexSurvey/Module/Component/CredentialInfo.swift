//
//  CredentialInfo.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 15/03/24.
//

import SwiftUI

struct CredentialInfo: View {
    var body: some View {
        VStack {
            if let name = UserDefaults.standard.string(forKey: "name"),
               let role = UserDefaults.standard.string(forKey: "role")
            {
                ColorMenuCard(color: LinearGradient.surveyBackground, minHeight: 0, alignment: .leading) {
                    HStack {
                        Text(name)
                            .surveyTitle()
                        Spacer()
                    }
                    Text(role)
                        .surveyDesc()
                }
            }
        }
    }
}
#Preview {
    CredentialInfo()
}
