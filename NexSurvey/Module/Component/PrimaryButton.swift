//
//  PrimaryButton.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 12/03/24.
//

import SwiftUI

struct PrimaryButton: View {
    var title: String
    
    var body: some View {
        Text(title)
            .font(.title3)
            .fontWeight(.bold)
            .foregroundColor(.white)
            .frame(maxWidth: .infinity)
            .padding()
            .background(Color.orange)
            .cornerRadius(50)
    }
}

#Preview {
    PrimaryButton(title: "Login")
}
