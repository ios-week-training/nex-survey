//
//  ColorMenuCard.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import SwiftUI

struct ColorMenuCard<Content: View>: View {
    var color: LinearGradient
    var minHeight: Double
    var alignment: HorizontalAlignment
    @ViewBuilder let contents: Content
    
    var body: some View {
        VStack(alignment: alignment, spacing: 9) {
            contents
        }
        .padding()
        .frame(maxWidth: .infinity, minHeight: minHeight)
        .clipped()
        .background {
            RoundedRectangle(cornerRadius: 14, style: .continuous)
                .fill(color)
                .shadow(color: Color(.sRGB, red: 0/255, green: 0/255, blue: 0/255).opacity(0.06), radius: 8, x: 0, y: 4)
        }
        .padding(.horizontal)
    }
}

#Preview {
    ColorMenuCard(color: LinearGradient.customBlue, minHeight: 100, alignment: .center) {
        Text("a")
    }
}
