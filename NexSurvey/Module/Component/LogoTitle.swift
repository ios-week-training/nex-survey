//
//  LogoTitle.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

struct LogoTitle: View {
    @Environment(\.dismiss) var dismiss
    @State var isLogout: Bool = false
    
    var body: some View {
        HStack {
            Image("Logo")
                .resizable()
                .frame(width: 170, height: 50)
            Spacer()
            Text("Logout")
                .foregroundStyle(Color.red)
                .bold()
                .onTapGesture {
                    isLogout.toggle()
                }
                .alert("Are you sure want to logout", isPresented: $isLogout) {
                    Button("logout") {
                        dismiss()
                        Auth.shared.logout()
                    }
                    Button("Cancel", role: .cancel) { }
                }
        }
        .padding(.vertical)
        .padding(.horizontal, 25)
    }
}

#Preview {
    LogoTitle()
}
