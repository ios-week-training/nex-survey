//
//  ContentView.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

let surveyUseCase: Interactor<
    String,
    [SurveyModel],
    GetAllSurveyRepository<
        SurveyLocaleDataSource,
        SurveyRemoteDataSource,
        SurveyTransformer>
> = injection.provideSurveyGetAll()

let surveyQuestionUseCase: Interactor<
    String,
    [PertanyaanEntity],
    GetSurveyQuestionsRepository<
        SurveyQuestionLocaleDataSource>
> = injection.provideSurveyGetQuestion()

let answerListUseCase: Interactor<
    String,
    [RespondenAnswerModel],
    GetAllAnswerRepository<
        AnswerLocaleDataSource,
        AnswerTransformer>
> = injection.provideAnswerGetAll()

let answerSaveUseCase: Interactor<
    RespondenAnswerModel,
    Bool,
    SaveAnswerRepository<
        AnswerLocaleDataSource,
        SurveyQuestionLocaleDataSource,
        AnswerTransformer>
> = injection.provideAnswerSave()

let answerDeleteUseCase: Interactor<
    String,
    Bool,
    DeleteAnswerRepository<
        AnswerLocaleDataSource>
> = injection.provideAnswerDelete()

let journeyGetAllUseCase: Interactor<
    String,
    [JourneyModel],
    GetAllJourneyRepository<
        JourneyLocaleDataSource,
        JourneyTransformer>
> = injection.provideJourneyGetAll()

let journeySaveUseCase: Interactor<
    JourneyModel,
    Bool,
    SaveJourneyRepository<
        JourneyLocaleDataSource,
        JourneyTransformer>
> = injection.provideJourveySave()

let journeyDeleteUseCase: Interactor<
    String,
    Bool,
    DeleteJourneyRepository<
        JourneyLocaleDataSource>
> = injection.provideJourneyDelete()

struct ContentView: View {
    var body: some View {
        MainScreen()
    }
}

#Preview {
    ContentView()
}
