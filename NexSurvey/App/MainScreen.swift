//
//  LoadingScreen.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

let loginUseCase: Interactor<
    LoginRequest,
    String,
    LoginRepository<
        LoginRemoteDataSource>
> = injection.provideLogin()

struct MainScreen: View {
    let loginPresenter = LoginPresenter(login: loginUseCase)
    @State var isActive: Bool = false
    
    var body: some View {
        ZStack {
            if self.isActive {
                LoginView(presenter: loginPresenter)
            } else {
                SplashScreen()
            }
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                withAnimation {
                    self.isActive = true
                }
            }
        }
    }
}

#Preview {
    MainScreen()
}
