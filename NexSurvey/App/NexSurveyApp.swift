//
//  NexSurveyApp.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import SwiftUI

let injection = Injection()

@main
struct NexSurveyApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
