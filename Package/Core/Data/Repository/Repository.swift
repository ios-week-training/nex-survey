//
//  Repository.swift
//  
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Combine

public protocol Repository {
  associatedtype Request
  associatedtype Response
  
  func execute(request: Request?) -> AnyPublisher<Response, Error>
}
