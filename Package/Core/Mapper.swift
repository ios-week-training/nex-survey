//
//  Mapper.swift
//  
//
//  Created by Gilang Ramadhan on 01/12/22.
//

import Foundation

public protocol Mapper {
  associatedtype Request
  associatedtype Response
  associatedtype Entity
  associatedtype Domain
  
  func transformResponseToEntity(request: Request?, response: Response) -> Entity
  func transformEntityToDomain(entity: Entity) -> Domain
  func transformDomainToEntity(domain: Domain) -> Entity
}
