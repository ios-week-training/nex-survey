//
//  Usecase.swift
//  
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import Combine

public protocol UseCase {
  associatedtype Request
  associatedtype Response
  
  func execute(request: Request?) -> AnyPublisher<Response, Error>
}
