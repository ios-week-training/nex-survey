//
//  ReverseMapper.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import Foundation

public protocol ReverseMapper {
    associatedtype Request
    associatedtype Entity
    associatedtype Domain
    
    func transformDomainToEntity(domain: Domain, request: Request?) -> Entity
}
