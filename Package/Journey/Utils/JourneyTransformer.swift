//
//  JourneyTransformer.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import RealmSwift
import CoreLocation

public struct JourneyTransformer: Mapper {
    public typealias Request = String
    public typealias Response = String
    public typealias Entity = JourneyEntity
    public typealias Domain = JourneyModel
    
    public func transformResponseToEntity(request: String?, response: String) -> JourneyEntity {
        return JourneyEntity()
    }
    
    public func transformEntityToDomain(entity: JourneyEntity) -> JourneyModel {
        return JourneyModel(id: entity.id, latitude: CLLocationDegrees(entity.latitude) ?? 0, longitude: CLLocationDegrees(entity.longitude) ?? 0, date: entity.date
        )
    }
    
    public func transformDomainToEntity(domain: Domain) -> Entity {
        let result = JourneyEntity()
        result.id = domain.id
        result.latitude = String(domain.latitude)
        result.longitude = String(domain.longitude)
        result.date = domain.date
        return result
    }
}
