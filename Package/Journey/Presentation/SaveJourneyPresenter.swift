//
//  SaveJourneyPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 22/03/24.
//

import Foundation
import Combine
import CoreLocation

public class SaveJourneyPresenter<saveJourneyUseCase: UseCase>: ObservableObject
where
saveJourneyUseCase.Request == JourneyModel,
saveJourneyUseCase.Response == Bool
{
    private var cancellables: Set<AnyCancellable> = []
    private let _saveJourneyUseCase: saveJourneyUseCase
    
    @Published public var result: saveJourneyUseCase.Response = false
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(saveJourney: saveJourneyUseCase) {
        self._saveJourneyUseCase = saveJourney
    }
    
    public func saveJourney(date: Date, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        isLoading = true
        let request = JourneyModel(id: UUID(), latitude: latitude, longitude: longitude, date: date)
        self._saveJourneyUseCase.execute(request: request)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.isLoading = false
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.result = item
            })
            .store(in: &cancellables)
    }
}
