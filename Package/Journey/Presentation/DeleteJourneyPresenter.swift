//
//  DeleteJourney.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 25/03/24.
//

import Foundation
import Combine

public class DeleteJourneyPresenter<deleteJourneyUseCase: UseCase>: ObservableObject
where
deleteJourneyUseCase.Request == String,
deleteJourneyUseCase.Response == Bool
{
    private var cancellables: Set<AnyCancellable> = []
    private let _deleteJourneyUseCase: deleteJourneyUseCase
    
    @Published public var result: deleteJourneyUseCase.Response = false
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(deleteJourney: deleteJourneyUseCase) {
        self._deleteJourneyUseCase = deleteJourney
    }
    
    public func deleteJourney() {
        isLoading = true
        self._deleteJourneyUseCase.execute(request: "")
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.isLoading = false
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.result = item
            })
            .store(in: &cancellables)
    }
}
