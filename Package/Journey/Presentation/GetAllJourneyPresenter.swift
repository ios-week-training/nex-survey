//
//  JourneyPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import Combine
import MapKit

public class GetAllJourneyPresenter<getAllJourneyUseCase: UseCase>: ObservableObject
where
getAllJourneyUseCase.Request == String,
getAllJourneyUseCase.Response == [JourneyModel]
{
    private var cancellables: Set<AnyCancellable> = []
    private let _getAllJourneyUseCase: getAllJourneyUseCase
    
    @Published public var result = getAllJourneyUseCase.Response()
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    @Published public var selectedRoute: CLLocationCoordinate2D?
    @Published public var route = [MKRoute]()
    
    public init(getAllJourney: getAllJourneyUseCase) {
        self._getAllJourneyUseCase = getAllJourney
    }
    
    public func getAllJourney(completion: @escaping (_ success: Bool) -> Void) {
        isLoading = true
        self._getAllJourneyUseCase.execute(request: "")
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completionStatus in
                switch completionStatus {
                case .finished:
                    self.isLoading = false
                    completion(true)
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                    completion(false)
                }
            }, receiveValue: { item in
                self.result = item
            })
            .store(in: &cancellables)
    }
    
    public func calculateRoute() {
        let start = result.count
        self.result.removeAll()
        
        getAllJourney() { [weak self] success in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                let total = self.result.count
                if total > 1 {
                    Task {
                        for i in start..<(total - 1) {
                            let req = MKDirections.Request()
                            req.source = MKMapItem(placemark:
                                MKPlacemark(
                                    coordinate: CLLocationCoordinate2D(
                                        latitude: self.result[i].latitude,
                                        longitude: self.result[i].longitude
                                    )
                                )
                            )
                            req.destination = MKMapItem(placemark:
                                MKPlacemark(
                                    coordinate: CLLocationCoordinate2D(
                                        latitude: self.result[i+1].latitude,
                                        longitude: self.result[i+1].longitude
                                    )
                                )
                            )
                            let direction = MKDirections(request: req)
                            let response = try await direction.calculate()
                            if let routes = response.routes.first {
                                self.route.append(routes)
                            }
                        }
                        self.selectedRoute = CLLocationCoordinate2D(latitude: self.result[total-1].latitude, longitude: self.result[total-1].longitude)
                    }
                }
            }
            
        }
    }
}
