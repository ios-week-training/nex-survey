//
//  DeleteJourneyRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 25/03/24.
//

import Foundation
import Combine

public struct DeleteJourneyRepository<
    journeyLocaleDataSource: LocaleDataSource>: Repository
where
journeyLocaleDataSource.Request == String,
journeyLocaleDataSource.Response == JourneyEntity
{
    
    public typealias Request = String
    public typealias Response = Bool
    
    private let _journeyLocaleDataSource: journeyLocaleDataSource
    
    public init(journeyDeleteLocale: journeyLocaleDataSource) {
        self._journeyLocaleDataSource = journeyDeleteLocale
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(error: LoginError.message("Invalid Request")).eraseToAnyPublisher() }
        return _journeyLocaleDataSource.delete(id: request)
            .eraseToAnyPublisher()
    }
}
