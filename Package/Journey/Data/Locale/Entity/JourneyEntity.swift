//
//  JourneyEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import RealmSwift
import CoreLocation

public class JourneyEntity: Object, Identifiable {
    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var latitude: String
    @Persisted var longitude: String
    @Persisted var date: Date
}
