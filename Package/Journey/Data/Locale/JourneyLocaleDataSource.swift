//
//  JourneyLocaleDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import Combine
import RealmSwift

public struct JourneyLocaleDataSource: LocaleDataSource {
    public typealias Request = String
    public typealias Response = JourneyEntity
    
    private let _realm: Realm
    
    public init(_realm: Realm) {
        self._realm = _realm
    }
    
    public func list(request: Request?) -> AnyPublisher<[Response], any Error> {
        return Future<[Response], Error> { completion in
            
            let surveyEntities = {
                self._realm.objects(JourneyEntity.self)
            }()
            completion(.success(Array(surveyEntities)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: Response) -> AnyPublisher<Bool, any Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entities)
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: Request) -> AnyPublisher<Response, any Error> {
        return Future<Response, Error> { completion in
            guard let uuid = UUID(uuidString: id) else {
                completion(.failure(LoginError.message("Invalid UUID format")))
                return
            }
            if let journeyRes = {
                self._realm.object(ofType: JourneyEntity.self, forPrimaryKey: uuid as CVarArg)
            }() {
                completion(.success(journeyRes))
            } else {
                completion(.failure(LoginError.message("Data not found")))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: Request, entity: Response) -> AnyPublisher<Bool, any Error> {
        return Future<Bool, Error> { completion in
            completion(.failure(DatabaseError.invalidInstance))
        }
        .eraseToAnyPublisher()
    }
    
    public func delete(id: Request) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            let journeyEntities = {
                self._realm.objects(JourneyEntity.self)
            }
            for i in Array(arrayLiteral: journeyEntities) {
                do {
                    try self._realm.write {
                        self._realm.delete(i())
                    }
                    completion(.success(true))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            }
        }.eraseToAnyPublisher()
    }
    
}
