//
//  JourneyGetAllRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import Combine

public struct GetAllJourneyRepository<
    journeyLocaleDataSource: LocaleDataSource,
    Transformer: Mapper>: Repository
where
journeyLocaleDataSource.Request == String,
journeyLocaleDataSource.Response == JourneyEntity,
Transformer.Request == String,
Transformer.Response == String,
Transformer.Entity == JourneyEntity,
Transformer.Domain == JourneyModel
{
    public typealias Request = String
    public typealias Response = [JourneyModel]
    
    private let _journeyLocaleDataSource: journeyLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        journeyGetLocale: journeyLocaleDataSource,
        mapper: Transformer
    ) {
        self._journeyLocaleDataSource = journeyGetLocale
        self._mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        return _journeyLocaleDataSource.list(request: "")
            .map({ item in
                item.map { data in
                    _mapper.transformEntityToDomain(entity: data)
                }
            })
            .eraseToAnyPublisher()
    }
}
