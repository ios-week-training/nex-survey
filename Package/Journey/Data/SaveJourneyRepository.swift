//
//  SaveJourneyRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 22/03/24.
//

import Foundation
import Combine

public struct SaveJourneyRepository<
    journeyLocaleDataSource: LocaleDataSource,
    Transformer: Mapper>: Repository
where
journeyLocaleDataSource.Request == String,
journeyLocaleDataSource.Response == JourneyEntity,
Transformer.Request == String,
Transformer.Response == String,
Transformer.Entity == JourneyEntity,
Transformer.Domain == JourneyModel
{
    public typealias Request = JourneyModel
    public typealias Response = Bool
    
    private let _journeyLocaleDataSource: journeyLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        journeyGetLocale: journeyLocaleDataSource,
        mapper: Transformer
    ) {
        self._journeyLocaleDataSource = journeyGetLocale
        self._mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(error: LoginError.message("Invalid Request")).eraseToAnyPublisher() }
        return _journeyLocaleDataSource.add(
            entities: _mapper.transformDomainToEntity(domain: request)
            )
            .eraseToAnyPublisher()
    }
}
