//
//  JourneyModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import CoreLocation

public struct JourneyModel: Identifiable {
    public let id: UUID
    public let latitude, longitude: CLLocationDegrees
    public let date: Date
}
