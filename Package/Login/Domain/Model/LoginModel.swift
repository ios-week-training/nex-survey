//
//  LoginModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation

public struct LoginModel: Identifiable {
    public var id: UUID
    public var name: String
    public var gender: String
    public var age: Int
    public var role: String
    public var company: String
    public var token: String
}
