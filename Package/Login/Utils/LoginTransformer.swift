//
//  IngredientTransformer.swift
//
//
//  Created by Fandy Gotama on 18/10/20.
//

//import Core
import RealmSwift
import Foundation

public struct LoginTransformer: Mapper {
    public typealias Request = String
    public typealias Response = LoginResponse
    public typealias Entity = LoginEntity
    public typealias Domain = LoginModel
    
    public init() { }
    
    public func transformResponseToEntity(request: Request?, response: Response) -> Entity {
        guard let data = response.data else { return LoginEntity() }
        let result = LoginEntity()
        result.id = UUID()
        result.name = data.detail.name
        result.gender = data.detail.gander
        result.age = data.detail.age
        result.role = data.detail.role
        result.company = data.detail.company
        result.token = data.token
        return result
    }
    
    public func transformEntityToDomain(entity: Entity) -> Domain {
        return LoginModel(
            id: entity.id, name: entity.name, gender: entity.gender, age: entity.age, role: entity.role, company: entity.company, token: entity.token)
    }
    
    public func transformDomainToEntity(domain: LoginModel) -> LoginEntity {
        let result = LoginEntity()
        result.id = domain.id
        result.name = domain.name
        result.gender = domain.gender
        result.age = domain.age
        result.role = domain.role
        result.company = domain.company
        result.token = domain.token
        return result
    }
    
}
