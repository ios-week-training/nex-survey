//
//  MealPresenter.swift
//
//
//  Created by Fandy Gotama on 19/10/20.
//

import Foundation
import Combine

public class LoginPresenter<loginUseCase: UseCase>: ObservableObject where
loginUseCase.Request == LoginRequest,
loginUseCase.Response == String
{
    
    private var cancellables: Set<AnyCancellable> = []
    private let _loginUseCase: loginUseCase
//    private let _loginLocaleUseCase: loginLocaleUseCase
    
    @Published public var username: String = ""
    @Published public var password: String = ""
    @Published public var isLogin: Bool = false
    @Published public var result: loginUseCase.Response?
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(login: loginUseCase) {
        _loginUseCase = login
    }
    
    public func postLogin() {
        isLoading = true
        let request = LoginRequest(username: username, password: password)
        self._loginUseCase.execute(request: request)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure (let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.validateLogin(item)
            })
            .store(in: &cancellables)
    }
    
    func validateLogin(_ status: String) {
        if status == "success" {
            isLogin = Auth.shared.loggedIn
            UserDefaults.standard.set(username, forKey: "username")
        } else {
            errorMessage = status
        }
    }
}
