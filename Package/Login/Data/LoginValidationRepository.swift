//
//  LoginTransformRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 15/03/24.
//

import Foundation
import Combine

public struct LoginValidationRepository<
    loginLocaleDataSource: LocaleDataSource,
    Transformer: Mapper
>: Repository where
loginLocaleDataSource.Request == String,
loginLocaleDataSource.Response == LoginEntity,
Transformer.Request == String,
Transformer.Response == LoginResponse,
Transformer.Entity == LoginEntity,
Transformer.Domain == LoginModel
{
    
    public typealias Request = LoginResponse
    public typealias Response = Bool
    
    private let _loginLocaleDataSource: loginLocaleDataSource
    private let mapper: Transformer
    
    public init(
        localeDataSource: loginLocaleDataSource,
        mapper: Transformer
    ) {
        self._loginLocaleDataSource = localeDataSource
        self.mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request, let requestDetailName = request.data?.detail.name else {
            return Fail(error: DatabaseError.requestFailed).eraseToAnyPublisher()
        }
        
        return _loginLocaleDataSource.list(request: nil)
            .flatMap { response -> AnyPublisher<Bool, Error> in
                if let firstItem = response.first(where: { $0.name == requestDetailName }) {
                    
                    return self._loginLocaleDataSource.delete(id: firstItem.id.uuidString)
                        .flatMap { _ in
                            self._loginLocaleDataSource.add(
                                entities: mapper.transformResponseToEntity(request: nil, response: request)
                            )
                        }
                        .map { _ in true }
                        .eraseToAnyPublisher()
                } else {
                    
                    return self._loginLocaleDataSource.add(
                            entities: mapper.transformResponseToEntity(request: nil, response: request)
                        )
                        .map { _ in true }
                        .eraseToAnyPublisher()
                }
            }
            .catch { _ in Just(false).setFailureType(to: Error.self).eraseToAnyPublisher() }
            .eraseToAnyPublisher()
    }
}
