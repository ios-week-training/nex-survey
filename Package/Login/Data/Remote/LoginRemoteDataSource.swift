//
//  LoginRemoteDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import Combine
import Alamofire

struct LoginRemoteDataSource: DataSource {
    typealias Request = LoginRequest
    typealias Response = LoginResponse
    
    private let endpoint: String
    
    public init(endpoint: String) {
        self.endpoint = endpoint
    }
    
    func execute(request: Request?) -> AnyPublisher<Response, Error> {
        return Future<Response, Error> { completion in
            guard let request = request else { return completion(.failure(URLError.invalidRequest) )}
            
            if let url = URL(string: self.endpoint) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = "POST"
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                do {
                    urlRequest.httpBody = try JSONEncoder().encode(request)
                } catch {
                    return completion(.failure(URLError.invalidRequest))
                }
                
                AF.request(urlRequest)
                    .responseData { response in
                        guard let data = response.data else { return }
                        
                        let decoder = JSONDecoder()
                        
                        do {
                            let loginRes = try decoder.decode(LoginResponse.self, from: data)
                            completion(.success(loginRes))
                        } catch {
                            completion(.failure(DatabaseError.invalidInstance))
                        }
                    }
            }
        }.eraseToAnyPublisher()
    }
}
