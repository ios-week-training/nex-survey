//
//  LoginRequest.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation

public struct LoginRequest: Encodable {
    let username: String
    let password: String
}
