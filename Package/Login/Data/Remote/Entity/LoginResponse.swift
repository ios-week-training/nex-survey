//
//  LoginResponse.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation

// MARK: - Login
public struct LoginResponse: Codable {
    let code: Int?
    let data: DataClass?
    let description: String?
    let error: ErrorLogin?
}

// MARK: - DataClass
struct DataClass: Codable {
    let detail: Detail
    let token: String
}

// MARK: - Detail
struct Detail: Codable {
    let name, gander: String
    let age: Int
    let role, company: String

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case gander = "Gander"
        case age = "Age"
        case role = "Role"
        case company = "Company"
    }
}

// MARK: - Error
struct ErrorLogin: Codable {
    let name, message, header: String
}

