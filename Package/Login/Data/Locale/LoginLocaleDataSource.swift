//
//  GetMealsLocaleDataSource.swift
//
//
//  Created by Fandy Gotama on 18/10/20.
//

//import Core
import Combine
import RealmSwift
import Foundation

public struct LoginLocaleDataSource : LocaleDataSource {
    public typealias Request = String?
    public typealias Response = LoginEntity
    
    private let _realm: Realm
    
    public init(realm: Realm) {
        self._realm = realm
    }
    
    public func list(request: String??) -> AnyPublisher<[Response], Error> {
        return Future<[Response], Error> { completion in
            
            let loginEntities = {
                self._realm.objects(LoginEntity.self)
            }()
            completion(.success(Array(loginEntities)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entities)
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<Response, Error> {
        return Future<Response, Error> { completion in
            if let loginRes = {
                self._realm.objects(LoginEntity.self).filter("id = '\(id)'")
            }().first {
                completion(.success(loginRes))
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: LoginEntity) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            completion(.failure(DatabaseError.invalidInstance))
        }
        .eraseToAnyPublisher()
    }
    
    public func delete(id: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            if let loginRes = {
                self._realm.objects(LoginEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try self._realm.write {
                        self._realm.delete(loginRes)
                    }
                    completion(.success(true))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
}
