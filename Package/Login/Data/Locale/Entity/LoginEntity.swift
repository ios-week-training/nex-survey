//
//  LoginEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import RealmSwift

public class LoginEntity: Object, Identifiable {
    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var name: String
    @Persisted var gender: String
    @Persisted var age: Int
    @Persisted var role: String
    @Persisted var company: String
    @Persisted var token: String
}
