//
//  LoginRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 14/03/24.
//

import Foundation
import Combine

public struct LoginRepository<
  loginRemoteDataSource: DataSource
>: Repository where
loginRemoteDataSource.Request == LoginRequest,
loginRemoteDataSource.Response == LoginResponse
{

  public typealias Request = LoginRequest
  public typealias Response = String
    
  private let _loginRemoteDataSource: loginRemoteDataSource

  public init(
    remoteDataSource: loginRemoteDataSource
  ) {
    self._loginRemoteDataSource = remoteDataSource
  }

    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(outputType: String.self, failure: LoginError.message("Error inisiate")).eraseToAnyPublisher() }
        
        return _loginRemoteDataSource.execute(request: request)
            .map { item in
                return validateLogin(item)
            }
            .eraseToAnyPublisher()
    }
    
    public func validateLogin(_ res: LoginResponse) -> String {
        switch res.code {
        case 200 :
            guard let data = res.data else { return "Failed" }
            Auth.shared.setCredentials(accessToken: data.token)
            UserDefaults.standard.set(data.detail.name, forKey: "name")
            UserDefaults.standard.set("\(data.detail.company), \(data.detail.role)", forKey: "role")
            return "success"
        case 401 :
            return res.description ?? ""
        case 404 :
            return res.description ?? ""
        case .none:
            return "Request timeout"
        case .some(_):
            return "Request timeout"
        }
    }
}
