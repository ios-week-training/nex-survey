//
//  GetAnswerPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import Combine

public class GetAllAnswerPresenter<getAllAnswerUseCase: UseCase>: ObservableObject
where
getAllAnswerUseCase.Request == String,
getAllAnswerUseCase.Response == [RespondenAnswerModel]
{
    private var cancellables: Set<AnyCancellable> = []
    private let _getAllAnswerUseCase: getAllAnswerUseCase
    
    @Published public var result = getAllAnswerUseCase.Response()
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(getAllAnswer: getAllAnswerUseCase) {
        self._getAllAnswerUseCase = getAllAnswer
    }
    
    public func getAllAnswer() {
        isLoading = true
        self._getAllAnswerUseCase.execute(request:  UserDefaults.standard.string(forKey: "username"))
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.isLoading = false
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.result = item
            })
            .store(in: &cancellables)
    }
}
