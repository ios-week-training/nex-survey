//
//  SurveySaveRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import Foundation
import Combine
import UIKit

public class SaveAnswerPresenter<answerUseCase: UseCase>: ObservableObject
where
answerUseCase.Request == RespondenAnswerModel,
answerUseCase.Response == Bool
{
    private var cancellables: Set<AnyCancellable> = []
    private let _answerUseCase: answerUseCase
    
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    @Published public var isSuccess: Bool = false
    
    public init(answer: answerUseCase) {
        self._answerUseCase = answer
    }
    
    public func saveAnswer(survey: [SurveyModel], stringAnswer: [String: String], checkedAnswer: [String: Bool], imageAnswer: [String: UIImage?]) {
        
        isLoading = true
        self._answerUseCase.execute(request: createAnswer(survey: survey, stringAnswer: stringAnswer, checkedAnswer: checkedAnswer, imageAnswer: imageAnswer))
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.isLoading = false
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.isSuccess = item
            })
            .store(in: &cancellables)
    }
    
    private func createAnswer(survey: [SurveyModel], stringAnswer: [String: String], checkedAnswer: [String: Bool], imageAnswer: [String: UIImage?]) -> RespondenAnswerModel {
        let uuid = UUID()
        return RespondenAnswerModel(
            id: uuid, name: stringAnswer["name", default: ""], age: stringAnswer["age", default: ""], date: Date(), 
            surveyList: createSurveyList(id: uuid, survey: survey, str: stringAnswer, bool: checkedAnswer, image: imageAnswer))
    }
    
    private func createSurveyList(id: UUID, survey: [SurveyModel], str:  [String: String], bool: [String: Bool], image: [String: UIImage?]) -> [SurveyAnswerModel] {
        var result = [SurveyAnswerModel]()
        var idImage: Int = 0
        for sur in survey {
            let surveyAnswer = SurveyAnswerModel(
                surveyQuestion: sur.judul, surveyDesc: sur.deskripsi, 
                answerList: createQuestionList(id: id, idImage: idImage, question: sur.pertanyaan, str: str, bool: bool, image: image))
            result.append(surveyAnswer)
            idImage += 1
        }
        return result
    }
    
    private func createQuestionList(id: UUID, idImage: Int, question: [PertanyaanModel], str: [String: String], bool: [String: Bool], image: [String: UIImage?]) -> [AnswerModel] {
        var result = [AnswerModel]()
        var idImageInside: Int = 1
        for ques in question {
            switch ques.jenis {
            case .checkbox:
                let answer = AnswerModel(
                    id: ques.id, question: ques, answer: findAllAnswer(question: ques.pertanyaan, list: ques.opsi ?? [], answer: bool))
                result.append(answer)
            case .radio:
                let answer = AnswerModel(
                    id: ques.id, question: ques, answer: findAllAnswer(question: ques.pertanyaan, list: ques.opsi ?? [], answer: bool))
                result.append(answer)
            case .camera:
                let answer: AnswerModel
                let key = "\(ques.pertanyaan)"
                if let images = image[key] {
                    let imagePath = "\(id)-image\(idImage)\(idImageInside).png"
                    let save = FileUrl.baseUrl.appendingPathComponent(imagePath)
                    answer = AnswerModel(
                        id: ques.id, question: ques, answer: [imagePath])
                    save.saveImage(images)
                    idImageInside += 1
                } else {
                    answer = AnswerModel(
                        id: ques.id, question: ques, answer: ["default"])
                }
                result.append(answer)
            default :
                let key = "\(ques.pertanyaan)"
                let answer = AnswerModel(
                    id: ques.id, question: ques, answer: [str[key] ?? ""])
                result.append(answer)
            }
        }
        return result
    }
    
    private func findAllAnswer(question: String, list: [String], answer: [String: Bool]) -> [String] {
        var result = [String]()
        for item in list {
            let key = "\(question)-\(item)"
            if answer[key] == true {
                result.append(item)
            }
        }
        return result
    }
}
