//
//  DeleteAnswerPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 26/03/24.
//

import Foundation
import Combine

public class DeleteAnswerPresenter<answerUseCase: UseCase>: ObservableObject
where
answerUseCase.Request == String,
answerUseCase.Response == Bool
{
    private var cancellables: Set<AnyCancellable> = []
    private let _answerUseCase: answerUseCase
    
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    @Published public var isSuccess: Bool = false
    
    public init(answer: answerUseCase) {
        self._answerUseCase = answer
    }
    
    public func deleteAnswer(id: UUID, answer: [SurveyAnswerModel]) {
        isLoading = true
        self._answerUseCase.execute(request: id.uuidString)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    self.isLoading = false
                    self.deleteImage(answer: answer)
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.isSuccess = item
            })
            .store(in: &cancellables)
    }
    
    private func deleteImage(answer: [SurveyAnswerModel]) {
        var id = 0
        for item in answer {
            for i in item.answerList {
                var ids = 1
                for x in i.answer {
                    let key = "\(String(describing: i.id))-image\(id)\(ids).png"
                    if x == key {
                        let file: URL = FileUrl.baseUrl.appendingPathComponent(key)
                        file.deleteImage()
                        ids += 1
                    }
                }
                id += 1
            }
        }
    }
}
