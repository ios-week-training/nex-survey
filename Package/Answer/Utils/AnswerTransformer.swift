//
//  AnswerTransformer.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import RealmSwift

public struct AnswerTransformer: Mapper, ReverseMapper {
    public typealias Request = String
    public typealias Response = String
    public typealias Entity = RespondenAnswerEntity
    public typealias Domain = RespondenAnswerModel
    
    public func transformResponseToEntity(request: Request?, response: Response) -> Entity {
        return RespondenAnswerEntity()
    }
    
    public func transformEntityToDomain(entity: Entity) -> Domain {
        return RespondenAnswerModel(
            id: entity.id, name: entity.name, age: entity.age, date: entity.date, 
            surveyList: surveyAnswerListToArray(entity.surveyList))
    }
    
    public func transformDomainToEntity(domain: Domain, request: Request?) -> Entity {
        let resData = RespondenAnswerEntity()
        resData.id = domain.id
        resData.username = UserDefaults.standard.string(forKey: "username") ?? ""
        resData.name = domain.name
        resData.age = domain.age
        resData.surveyList = surveyAnswerArrayToList(array: domain.surveyList)
        return resData
    }
    
    public func transformDomainToEntity(domain: Domain) -> Entity {
        let resData = RespondenAnswerEntity()
        resData.id = domain.id
        resData.name = domain.name
        resData.age = domain.age
        resData.surveyList = surveyAnswerArrayToList(array: domain.surveyList)
        return resData
    }
    
//    normal
    private func surveyAnswerListToArray(_ list: List<SurveyAnswerEntity>) -> [SurveyAnswerModel] {
        var resData = [SurveyAnswerModel]()
        for itemData in list {
            let res = SurveyAnswerModel(
                surveyQuestion: itemData.surveyQuestion, surveyDesc: itemData.surveyDesc,
                answerList: answerListToArray(itemData.answerList))
            resData.append(res)
        }
        return resData
    }
    
    private func answerListToArray(_ list: List<AnswerEntity>) -> [AnswerModel] {
        var resData = [AnswerModel]()
        for itemData in list {
            let res = AnswerModel(
                id: UUID(), question: pertanyaanEntityToModel(itemData.question ?? PertanyaanEntity()),
                answer: Array(itemData.answer))
            resData.append(res)
        }
        return resData
    }
    
    private func pertanyaanEntityToModel(_ entity: PertanyaanEntity) -> PertanyaanModel {
        return PertanyaanModel(
            id: entity.id, jenis: JenisModel(rawValue: entity.jenis) ?? .freeText, pertanyaan: entity.pertanyaan, opsi: Array(entity.opsi), wajib: entity.wajib)
    }
    
//    reverse
    private func surveyAnswerArrayToList(array: [SurveyAnswerModel]) -> List<SurveyAnswerEntity> {
        let result = List<SurveyAnswerEntity>()
        for item in array {
            let list = SurveyAnswerEntity()
            list.surveyQuestion = item.surveyQuestion
            list.surveyDesc = item.surveyDesc
            list.answerList = answerArrayToList(array: item.answerList)
            result.append(list)
        }
        return result
    }
    
    private func answerArrayToList(array: [AnswerModel]) -> List<AnswerEntity> {
        let result = List<AnswerEntity>()
        for item in array {
            let list = AnswerEntity()
            if let data = item.question {
                list.question = questionModelToEntity(model: data)
            }
            list.answer.append(objectsIn: item.answer)
            result.append(list)
        }
        return result
    }
    
    private func questionModelToEntity(model: PertanyaanModel) -> PertanyaanEntity {
        let result = PertanyaanEntity()
        result.id = model.id
        return result
    }
}
