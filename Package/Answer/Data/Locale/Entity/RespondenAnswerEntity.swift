//
//  RespondenAnswerEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import RealmSwift

public class RespondenAnswerEntity: Object, Identifiable {
    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var username: String
    @Persisted var name: String
    @Persisted var age: String
    @Persisted var date: Date
    @Persisted var surveyList: List<SurveyAnswerEntity>
}
