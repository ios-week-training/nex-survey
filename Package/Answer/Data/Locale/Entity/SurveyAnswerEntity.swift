//
//  SurveyAnswerEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import RealmSwift

public class SurveyAnswerEntity: Object, Identifiable {
//    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var surveyQuestion: String
    @Persisted var surveyDesc: String
    @Persisted var answerList: List<AnswerEntity>
}
