//
//  AnswerEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import RealmSwift

public class AnswerEntity: Object, Identifiable {
//    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var question: PertanyaanEntity?
    @Persisted var answer: List<String>
}
