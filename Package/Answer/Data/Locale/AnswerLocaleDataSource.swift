//
//  AnswerLocaleDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import Combine
import RealmSwift

public struct AnswerLocaleDataSource: LocaleDataSource {
    public typealias Request = String?
    public typealias Response = RespondenAnswerEntity
    
    private let _realm: Realm
    
    init(_realm: Realm) {
        self._realm = _realm
    }
    
    public func list(request: String??) -> AnyPublisher<[Response], Error> {
        return Future<[Response], Error> { completion in
            
            let answerEntities = {
                self._realm.objects(RespondenAnswerEntity.self).sorted(byKeyPath: "date", ascending: false)
            }()
            completion(.success(Array(answerEntities)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entities)
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<RespondenAnswerEntity, Error> {
        return Future<Response, Error> { completion in
            if let answerRes = {
                self._realm.objects(RespondenAnswerEntity.self).filter("id = '\(id)'")
            }().first {
                completion(.success(answerRes))
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: RespondenAnswerEntity) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            completion(.failure(DatabaseError.invalidInstance))
        }
        .eraseToAnyPublisher()
    }
    
    public func delete(id: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            guard let uuid = UUID(uuidString: id) else {
                completion(.failure(LoginError.message("Invalid UUID format")))
                return
            }
            let predicate = NSPredicate(format: "id == %@", uuid as CVarArg)
            
            if let answerRes = {
                self._realm.objects(RespondenAnswerEntity.self).filter(predicate)
            }().first {
                do {
                    try self._realm.write {
                        self._realm.delete(answerRes)
                    }
                    completion(.success(true))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
}
