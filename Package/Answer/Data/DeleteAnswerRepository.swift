//
//  DeleteAnswerRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 26/03/24.
//

import Foundation
import Combine

public struct DeleteAnswerRepository<
    answerLocaleDataSource: LocaleDataSource>: Repository
where
answerLocaleDataSource.Request == String?,
answerLocaleDataSource.Response == RespondenAnswerEntity
{
    
    public typealias Request = String
    public typealias Response = Bool
    
    private let _answerLocaleDataSource: answerLocaleDataSource
    
    public init(answerLocale: answerLocaleDataSource) {
        self._answerLocaleDataSource = answerLocale
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(error: LoginError.message("Invalid Request")).eraseToAnyPublisher() }
        
        return _answerLocaleDataSource.delete(id: request)
    }
    
}
