//
//  GetAllAnswerrepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import Combine

public struct GetAllAnswerRepository<
    answerGetLocaleDataSource: LocaleDataSource,
    Transformer: Mapper>: Repository
where
answerGetLocaleDataSource.Request == String?,
answerGetLocaleDataSource.Response == RespondenAnswerEntity,
Transformer.Request == String,
Transformer.Response == String,
Transformer.Entity == RespondenAnswerEntity,
Transformer.Domain == RespondenAnswerModel
{
    
    public typealias Request = String
    public typealias Response = [RespondenAnswerModel]
    
    private let _answerGetLocaleDataSource: answerGetLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        answerGetLocale: answerGetLocaleDataSource,
        mapper: Transformer
    ) {
        self._answerGetLocaleDataSource = answerGetLocale
        self._mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        return _answerGetLocaleDataSource.list(request: request)
            .map { item in
                item.filter { $0.username == request }
            }
            .map({ item in
                item.map { data in
                    _mapper.transformEntityToDomain(entity: data)
                }
            })
            .eraseToAnyPublisher()
    }
}
