//
//  SaveAnswerRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import Foundation
import Combine

public struct SaveAnswerRepository<
    answerGetLocaleDataSource: LocaleDataSource,
    surveyQuestionLocaleDataSource: LocaleDataSource,
    Transformer: ReverseMapper>: Repository
where
answerGetLocaleDataSource.Request == String?,
answerGetLocaleDataSource.Response == RespondenAnswerEntity,
surveyQuestionLocaleDataSource.Request == String,
surveyQuestionLocaleDataSource.Response == PertanyaanEntity,
Transformer.Request == String,
Transformer.Entity == RespondenAnswerEntity,
Transformer.Domain == RespondenAnswerModel
{
    
    public typealias Request = RespondenAnswerModel
    public typealias Response = Bool
    
    private let _answerGetLocaleDataSource: answerGetLocaleDataSource
    private let _surveyQuestionLocaleDataSource: surveyQuestionLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        answerGetLocale: answerGetLocaleDataSource,
        questionGetLocale: surveyQuestionLocaleDataSource,
        mapper: Transformer
    ) {
        self._answerGetLocaleDataSource = answerGetLocale
        self._surveyQuestionLocaleDataSource = questionGetLocale
        self._mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(error: LoginError.message("Invalid Request")).eraseToAnyPublisher() }
        let data = _mapper.transformDomainToEntity(domain: request, request: "")
        
        let fetchOperations = data.surveyList.indices.flatMap { surveyIndex -> [AnyPublisher<(Int, Int, PertanyaanEntity), Error>] in
                data.surveyList[surveyIndex].answerList.indices.compactMap { answerIndex -> AnyPublisher<(Int, Int, PertanyaanEntity), Error>? in
                    guard let id = data.surveyList[surveyIndex].answerList[answerIndex].question?.id else { return nil }
                    return _surveyQuestionLocaleDataSource.get(id: id.uuidString)
                        .map { (surveyIndex, answerIndex, $0) }
                        .eraseToAnyPublisher()
                }
            }
        
        let fetchAndUpdate = Publishers.MergeMany(fetchOperations)
            .collect()
            .map { results -> RespondenAnswerEntity in
                let newData = data
                results.forEach { surveyIndex, answerIndex, fetchedQuestion in
                    newData.surveyList[surveyIndex].answerList[answerIndex].question = fetchedQuestion
                }
                return newData
            }
            .eraseToAnyPublisher()
        
        return fetchAndUpdate
            .flatMap { newData -> AnyPublisher<Bool, Error> in
                self._answerGetLocaleDataSource.add(entities: newData)
            }
            .eraseToAnyPublisher()
    }
}
