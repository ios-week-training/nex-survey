//
//  AnswerModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation

public struct AnswerModel: Identifiable {
    public var id: UUID
    public let question: PertanyaanModel?
    public let answer: [String]
}
