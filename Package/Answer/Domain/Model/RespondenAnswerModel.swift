//
//  RespondenAnswerModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation

public struct RespondenAnswerModel: Identifiable {
    public let id: UUID
    public let name, age: String
    public let date: Date
    public let surveyList: [SurveyAnswerModel]
}
