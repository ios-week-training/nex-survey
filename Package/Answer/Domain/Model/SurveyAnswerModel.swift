//
//  SurveyAnswerModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation

public struct SurveyAnswerModel {
    public let surveyQuestion, surveyDesc: String
    public let answerList: [AnswerModel]
}
