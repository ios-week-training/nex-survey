//
//  SurveyTransformer.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import RealmSwift

public struct SurveyTransformer: Mapper {
    
    public typealias Request = String
    public typealias Response = SurveyDataClass
    public typealias Entity = SurveyEntity
    public typealias Domain = SurveyModel
    
    public func transformResponseToEntity(request: Request?, response: Response) -> Entity {
        
        let resData = SurveyEntity()
        resData.id = UUID()
        resData.judul = response.judul
        resData.deskripsi = response.deskripsi
        
        response.pertanyaan.forEach { pertanyaan in
            let resPertanyaan = PertanyaanEntity()
            resPertanyaan.id = UUID()
            resPertanyaan.jenis = pertanyaan.jenis.rawValue
            resPertanyaan.pertanyaan = pertanyaan.pertanyaan
            resPertanyaan.wajib = pertanyaan.wajib
            
            pertanyaan.opsi?.forEach({ opsi in
                resPertanyaan.opsi.append(opsi)
            })
            resData.pertanyaan.append(resPertanyaan)
        }
        
        return resData
    }
    
    public func transformEntityToDomain(entity: Entity) -> Domain {
        let resData = SurveyModel(
            id: UUID(),
            judul: entity.judul,
            deskripsi: entity.deskripsi,
            pertanyaan: transformListToArray(list: entity.pertanyaan)
        )
        return resData
    }
    
    public func transformDomainToEntity(domain: Domain) -> Entity {
        let result = SurveyEntity()
        result.id = domain.id
        result.judul = domain.judul
        result.deskripsi = domain.deskripsi
        return result
    }
    
    private func transformListToArray(list: List<PertanyaanEntity>) -> [PertanyaanModel] {
        var reslist = [PertanyaanModel]()
        list.forEach { item in
            guard let jenisEnum = JenisModel(rawValue: item.jenis) else {
                print("Error: Invalid jenis value \(item.jenis)")
                return
            }
            
            let result = PertanyaanModel(
                id: item.id,
                jenis: jenisEnum,
                pertanyaan: item.pertanyaan,
                opsi: Array(item.opsi),
                wajib: item.wajib
            )
            
            reslist.append(result)
        }
        return reslist
    }
}
