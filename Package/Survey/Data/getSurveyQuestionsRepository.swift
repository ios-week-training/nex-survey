//
//  getSurveyQuestionsRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 21/03/24.
//

import Foundation
import Combine

public struct GetSurveyQuestionsRepository<
    surveyGetLocaleDataSource: LocaleDataSource>: Repository
where
surveyGetLocaleDataSource.Request == String,
surveyGetLocaleDataSource.Response == PertanyaanEntity
{
    
    public typealias Request = String
    public typealias Response = [PertanyaanEntity]
    
    private let _surveyGetLocaleDataSource: surveyGetLocaleDataSource
    
    public init(surveyGetLocale: surveyGetLocaleDataSource) {
        self._surveyGetLocaleDataSource = surveyGetLocale
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        guard let request = request else { return Fail(error: LoginError.message("Invalid Request")).eraseToAnyPublisher() }
        return _surveyGetLocaleDataSource.list(request: request)
    }
}
