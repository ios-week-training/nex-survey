//
//  SurveyLocaleDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 19/03/24.
//

import Foundation
import Combine
import RealmSwift

public struct SurveyLocaleDataSource: LocaleDataSource {
    public typealias Request = String?
    public typealias Response = SurveyEntity
    
    private let _realm: Realm
    
    init(_realm: Realm) {
        self._realm = _realm
    }
    
    public func list(request: String??) -> AnyPublisher<[Response], Error> {
        return Future<[Response], Error> { completion in
            
            let surveyEntities = {
                self._realm.objects(SurveyEntity.self)
            }()
            completion(.success(Array(surveyEntities)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entities)
                    for i in entities.pertanyaan {
                        self._realm.add(i)
                    }
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<Response, Error> {
        return Future<Response, Error> { completion in
            if let surveyRes = {
                self._realm.objects(SurveyEntity.self).filter("id = '\(id)'")
            }().first {
                completion(.success(surveyRes))
            } else {
                completion(.failure(LoginError.message("Data not found")))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            completion(.failure(DatabaseError.invalidInstance))
        }
        .eraseToAnyPublisher()
    }
    
    public func delete(id: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            if let surveyRes = {
                self._realm.objects(SurveyEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try self._realm.write {
                        self._realm.delete(surveyRes)
                    }
                    completion(.success(true))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
    
    
}
