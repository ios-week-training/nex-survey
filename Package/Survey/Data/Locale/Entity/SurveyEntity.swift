//
//  SurveyEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import RealmSwift

public class SurveyEntity: Object, Identifiable {
    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var judul: String
    @Persisted var deskripsi: String
    @Persisted var pertanyaan: List<PertanyaanEntity>
}

public enum JenisEnum: String, Codable {
    case camera = "camera"
    case checkbox = "checkbox"
    case dropdown = "dropdown"
    case freeText = "free_text"
    case radio = "radio"
    case shortAnswer = "short_answer"
}
