//
//  PertanyaanEntity.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import RealmSwift

public class PertanyaanEntity: Object, Identifiable {
    @Persisted(primaryKey: true) public var id: UUID
    @Persisted var jenis: String
    @Persisted var pertanyaan: String
    @Persisted var opsi: List<String>
    @Persisted var wajib: Bool
}
