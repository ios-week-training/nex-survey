//
//  SurveyQuestionLocaleDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 20/03/24.
//

import Foundation
import Combine
import RealmSwift

public struct SurveyQuestionLocaleDataSource: LocaleDataSource {
    public typealias Request = String
    public typealias Response = PertanyaanEntity
    
    private let _realm: Realm
    
    init(_realm: Realm) {
        self._realm = _realm
    }
    
    public func list(request: Request?) -> AnyPublisher<[Response], Error> {
        return Future<[Response], Error> { completion in
            let idx = Int(request ?? "0")
            
            let pertanyaanEntities = {
                self._realm.objects(SurveyEntity.self)
            }()
            let result = pertanyaanEntities[idx ?? 0].pertanyaan
            completion(.success(Array(result)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try self._realm.write {
                    self._realm.add(entities)
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<PertanyaanEntity, Error> {
        return Future<Response, Error> { completion in
            guard let uuid = UUID(uuidString: id) else {
                completion(.failure(LoginError.message("Invalid UUID format")))
                return
            }
            let predicate = NSPredicate(format: "id == %@", uuid as CVarArg)
            
            if let pertanyaanRes = {
                self._realm.objects(PertanyaanEntity.self).filter(predicate)
            }().first {
                completion(.success(pertanyaanRes))
            } else {
                completion(.failure(LoginError.message("Data not found")))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: Response) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            completion(.failure(DatabaseError.invalidInstance))
        }
        .eraseToAnyPublisher()
    }
    
    public func delete(id: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            if let pertanyaanRes = {
                self._realm.objects(PertanyaanEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try self._realm.write {
                        self._realm.delete(pertanyaanRes)
                    }
                    completion(.success(true))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
    
}
