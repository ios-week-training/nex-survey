//
//  GetAllSurveyRepository.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import Combine

public struct GetAllSurveyRepository<
    surveyGetLocaleDataSource: LocaleDataSource,
    surveyGetRemoteDataSource: DataSource,
    Transformer: Mapper> : Repository
where
surveyGetLocaleDataSource.Request == String?,
surveyGetLocaleDataSource.Response == SurveyEntity,
surveyGetRemoteDataSource.Request == String,
surveyGetRemoteDataSource.Response == SurveyResponse,
Transformer.Request == String,
Transformer.Response == SurveyDataClass,
Transformer.Entity == SurveyEntity,
Transformer.Domain == SurveyModel
{
    
    public typealias Request = String
    public typealias Response = [SurveyModel]
    
    private let _surveyGetRemoteDataSource: surveyGetRemoteDataSource
    private let _surveyGetLocaleDataSource : surveyGetLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        surveyGetLocale: surveyGetLocaleDataSource,
        surveyGetRemote: surveyGetRemoteDataSource,
        mapper: Transformer
    ) {
        self._surveyGetLocaleDataSource = surveyGetLocale
        self._surveyGetRemoteDataSource = surveyGetRemote
        self._mapper = mapper
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        return _surveyGetLocaleDataSource.list(request: request)
            .flatMap { result -> AnyPublisher<Response, Error> in
                if result.isEmpty {
                    return self.getfromRemote(request: request)
                } else {
                    let transformed = result.map { self._mapper.transformEntityToDomain(entity: $0) }
                    return Just(transformed).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }
    
    private func getfromRemote(request: Request?) -> AnyPublisher<Response, Error> {
        return _surveyGetRemoteDataSource.execute(request: request)
            .flatMap { response -> AnyPublisher<Response, Error> in
                var result = [SurveyEntity]()
                var returnResult = Response()
                
                switch response.data {
                case .dataClass(let dataClass):
                    let res1 = self._mapper.transformResponseToEntity(request: nil, response: dataClass)
                    let res2 = self._mapper.transformEntityToDomain(entity: res1)
                    result = [res1]
                    returnResult = [res2]
                case .datumArray(let datum):
                    let transformed = datum.map { data in
                        let res1 = self._mapper.transformResponseToEntity(request: nil, response: data.survei)
                        result.append(res1)
                        return self._mapper.transformEntityToDomain(entity: res1)
                    }
                    returnResult = transformed
                case .none:
                    return Just([]).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
                for resData in result {
                    _ = _surveyGetLocaleDataSource.add(entities: resData)
                }
                return Just(returnResult).setFailureType(to: Error.self).eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}

