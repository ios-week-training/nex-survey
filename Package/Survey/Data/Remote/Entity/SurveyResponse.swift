//
//  SurveyResponse.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 16/03/24.
//

import Foundation

// MARK: - SurveyResponse
public struct SurveyResponse: Codable {
    let code: Int
    let data: DataUnion?
    let description: String?
}

public enum DataUnion: Codable {
    case dataClass(SurveyDataClass)
    case datumArray([Datum])

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([Datum].self) {
            self = .datumArray(x)
            return
        }
        if let x = try? container.decode(SurveyDataClass.self) {
            self = .dataClass(x)
            return
        }
        throw DecodingError.typeMismatch(DataUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for DataUnion"))
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .dataClass(let x):
            try container.encode(x)
        case .datumArray(let x):
            try container.encode(x)
        }
    }
}

// MARK: - Datum
public struct Datum: Codable {
    let survei: SurveyDataClass
}

// MARK: - DataClass
public struct SurveyDataClass: Codable {
    let judul, deskripsi: String
    let pertanyaan: [Pertanyaan]
}

// MARK: - Pertanyaan
public struct Pertanyaan: Codable {
    let jenis: Jenis
    let pertanyaan: String
    let opsi: [String]?
    let wajib: Bool
}

public enum Jenis: String, Codable {
    case camera = "camera"
    case checkbox = "checkbox"
    case dropdown = "dropdown"
    case freeText = "free_text"
    case radio = "radio"
    case shortAnswer = "short_answer"
}
