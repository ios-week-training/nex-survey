//
//  SurveyRemoteDataSource.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import Combine
import Alamofire

struct SurveyRemoteDataSource: DataSource {
    typealias Request = String
    typealias Response = SurveyResponse
    
    private var endpoint: String
    
    public init(endpoint: String) {
        self.endpoint = endpoint
    }
    
    func execute(request: Request?) -> AnyPublisher<Response, Error> {
        return Future<Response, Error> { completion in
            guard request != nil else { return completion(.failure(URLError.invalidRequest) )}
            if let url = URL(string: self.endpoint) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = "GET"
                urlRequest.setValue("Token", forHTTPHeaderField: "thisIsRandomToken")
                
                AF.request(urlRequest)
                    .responseData { response in
                        guard let data = response.data else { return }
                        let decoder = JSONDecoder()
                        
                        do {
                            let surveyRes = try decoder.decode(SurveyResponse.self, from: data)
                            completion(.success(surveyRes))
                        } catch {
                            completion(.failure(DatabaseError.invalidInstance))
                        }
                    }
            }
        }.eraseToAnyPublisher()
    }
}
