//
//  SurveyGetAllPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import Combine

public class SurveyGetAllPresenter<surveyUseCase: UseCase>: ObservableObject
where
surveyUseCase.Request == String,
surveyUseCase.Response == [SurveyModel]
{
    
    private var cancellables: Set<AnyCancellable> = []
    private let _surveyUseCase: surveyUseCase
    
    @Published public var result = surveyUseCase.Response()
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(surveyGetAll: surveyUseCase) {
        _surveyUseCase = surveyGetAll
    }
    
    public func getAllSurvey() {
        isLoading = true
        self._surveyUseCase.execute(request: "")
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure (let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.result = item
            })
            .store(in: &cancellables)
    }
}
