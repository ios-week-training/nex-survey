//
//  SurveyPresenter.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation
import SwiftUI
import Combine

public class SurveyPresenter<surveyQuestionUseCase: UseCase>: ObservableObject
where
surveyQuestionUseCase.Request == String,
surveyQuestionUseCase.Response == [PertanyaanEntity]
{
    
    private var cancellables: Set<AnyCancellable> = []
    private let _surveyQuestionUseCase: surveyQuestionUseCase
    
    @Published var answer = [String: String]()
    @Published var checkedAnswer = [String: Bool]()
    @Published var dropPadding = [String: CGFloat]()
    @Published var imageAnswer = [String: UIImage?]()
    
    @Published var activePage: Int = 0
    @Published var isSubmited: Bool = false
    @Published var status: Bool = false
    var defaultImage = UIImage(named: "default")
    
    @Published public var result = surveyQuestionUseCase.Response()
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(surveyQuestion: surveyQuestionUseCase) {
        self._surveyQuestionUseCase = surveyQuestion
    }
    
    public func reset() {
        self.answer = [String: String]()
        self.checkedAnswer = [String: Bool]()
        self.dropPadding = [String: CGFloat]()
    }
    
    public func bindingAnser(for item: String) -> Binding<String> {
        Binding<String>(
            get: { self.answer[item, default: ""] },
            set: { self.answer[item] = $0 }
        )
    }
    
    public func bindingDropdown(for item: String) -> Binding<CGFloat> {
        Binding<CGFloat>(
            get: { self.dropPadding[item, default: 0] },
            set: { self.dropPadding[item] = $0 }
        )
    }
    
    public func bindingCheckbox(for item: String) -> Binding<Bool> {
        Binding<Bool>(
            get: { self.checkedAnswer[item, default: false] },
            set: { self.checkedAnswer[item] = $0 }
        )
    }
    
    public func bindingImage(for item: String) -> Binding<UIImage?> {
        Binding<UIImage?>(
            get: { self.imageAnswer[item, default: nil] },
            set: { self.imageAnswer[item] = $0 }
        )
    }
    
    public func changePage(_ act: Int) {
        if act >= 0 {
            checkMandatory { [weak self] success in
                guard let self = self else { return }
                
                DispatchQueue.main.async {
                    if success && !self.isLoading {
                        self.activePage += act
                    } else {
                        self.status = true
                    }
                }
            }
        } else {
            self.activePage += act
        }
    }
    
    public func setupAnswer() {
        checkMandatory { [weak self] success in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                if success && !self.isLoading {
                    self.isSubmited = true
                } else {
                    self.status = true
                }
            }
        }
    }
    
    private func checkMandatory(completion: @escaping (_ success: Bool) -> Void) {
        isLoading = true
        self._surveyQuestionUseCase.execute(request: "\(activePage)")
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure (let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { item in
                self.result = item
                completion(self.checkAnswer())
            })
            .store(in: &cancellables)
    }
    
    private func checkAnswer() -> Bool {
        if answer["name"] == nil || answer["name"] == "" {
            return false
        } else if answer["age"] == nil || answer["age"] == "" {
            return false
        } else {
            for item in result {
                if item.wajib {
                    var isFilled = false
                    switch JenisModel(rawValue: item.jenis) {
                    case .checkbox :
                        for op in item.opsi {
                            let key = "\(item.pertanyaan)-\(op)"
                            if checkedAnswer[key, default: false] {
                                isFilled = true
                                break
                            }
                        }
                        if !isFilled {
                            return false
                        }
                    case .radio :
                        for op in item.opsi {
                            let key = "\(item.pertanyaan)-\(op)"
                            if checkedAnswer[key, default: false] {
                                isFilled = true
                                break
                            }
                        }
                        if !isFilled {
                            return false
                        }
                    case .camera:
                        if imageAnswer[item.pertanyaan] == nil {
                            return false
                        }
                        break
                    default :
                        if answer[item.pertanyaan] == nil || answer[item.pertanyaan] == "" {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }
}
