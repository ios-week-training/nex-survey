//
//  DomainModel.swift
//  NexSurvey
//
//  Created by Iqbal Rahman on 18/03/24.
//

import Foundation

public struct SurveyModel: Identifiable {
    public let id: UUID
    public let judul, deskripsi: String
    public let pertanyaan: [PertanyaanModel]
}

public struct PertanyaanModel: Codable, Hashable {
    public let id: UUID
    let jenis: JenisModel
    let pertanyaan: String
    let opsi: [String]?
    let wajib: Bool
    
    init(id: UUID = UUID(), jenis: JenisModel = .freeText, pertanyaan: String = "", opsi: [String]? = [], wajib: Bool = false) {
        self.id = id
        self.jenis = jenis
        self.pertanyaan = pertanyaan
        self.opsi = opsi
        self.wajib = wajib
    }
}

public enum JenisModel: String, Codable {
    case camera = "camera"
    case checkbox = "checkbox"
    case dropdown = "dropdown"
    case freeText = "free_text"
    case radio = "radio"
    case shortAnswer = "short_answer"
}
